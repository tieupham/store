Project Store

- Mô tả: là một trang web bán hàng gồm các chức năng cơ bản:
	-Front
		+đăng kí, đăng nhập
		+tìm kiếm
		+comment
		+shooping cart
		+thanh toán
		+.....
		
	-Admin:
		+CRUD 
			+thể loại
			+sản phẩm
			+nhà sản xuất 
			+user
		+Theo dõi các đơn hàng của khách hàng
- Công nghệ, tài liệu tham khảo:
	+ PHP theo mô hình MVC
	+ .htaccess tại <a href="https://freetuts.net/hoc-php/file-htaccess">freetuts</a>
	+ PHPMailer