-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 16, 2017 at 04:01 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phpmvc`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `name`, `deleted`) VALUES
(1, 0, 'Laptop', 0),
(2, 1, 'Didong', 0),
(3, 1, 'Samsung', 0),
(4, 3, 'Samsung2', 0),
(5, 0, 'Phụ kiện', 0),
(6, 5, 'Tai nghe', 0),
(7, 5, 'Ốp lưng, bao da', 0),
(8, 5, 'Thẻ nhớ', 0),
(9, 5, 'Pin sạc dự phòng', 0),
(10, 5, 'Cáp sạc', 0),
(11, 5, 'USB', 0),
(12, 5, 'Loa', 0),
(13, 5, 'Phụ kiện khác', 0),
(14, 0, 'Điện thoại', 0),
(15, 0, 'Tablet', 0);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `product_id`, `user_id`, `email`, `user_name`, `content`, `created_on`, `deleted`) VALUES
(1, 28, NULL, NULL, 'anhnt', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2017-06-02 10:43:02', 0),
(2, 28, NULL, NULL, NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2017-06-02 10:43:02', 0),
(3, 28, NULL, NULL, NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2017-06-02 10:43:32', 0),
(4, 28, NULL, NULL, 'anhnt', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2017-06-02 10:43:02', 0),
(5, 28, NULL, 'duongduong@email.com', 'Duong Duong', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s', '2017-06-05 08:42:54', 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `deleted`) VALUES
(1, 'admin', 0),
(2, 'staff', 0),
(3, 'customer', 0);

-- --------------------------------------------------------

--
-- Table structure for table `login_log`
--

CREATE TABLE `login_log` (
  `id` int(11) NOT NULL,
  `ip` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `access_type` tinyint(4) DEFAULT NULL COMMENT '1:web, 2:ios, 3:android',
  `user_id` int(11) DEFAULT NULL,
  `login_result` tinyint(4) NOT NULL COMMENT '1:success,2 fail',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `login_log`
--

INSERT INTO `login_log` (`id`, `ip`, `access_type`, `user_id`, `login_result`, `created_on`, `deleted`) VALUES
(1, '::1', 1, 7, 1, '2017-06-11 03:00:58', 0),
(2, '::1', 1, 8, 1, '2017-06-11 03:09:16', 0),
(3, '::1', 1, 8, 1, '2017-06-11 17:20:29', 0),
(4, '::1', 1, 8, 1, '2017-06-12 01:04:35', 0);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `total_price` float NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_code`, `user_id`, `user_name`, `address`, `phone`, `total_price`, `status`, `created_on`, `deleted`) VALUES
(1, 'OD1496839782', 7, 'Nguyen The Anh', 'Dong Anh', '986238495', 27175000, 1, '2017-06-06 10:24:37', 0),
(2, 'OD1496839788', 7, 'Nguyen The Anh', 'Dong Anh', '986238495', 27175000, 1, '2017-06-02 10:24:37', 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

CREATE TABLE `order_detail` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `count` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_detail`
--

INSERT INTO `order_detail` (`id`, `order_id`, `product_id`, `price`, `count`, `deleted`) VALUES
(1, 1, 25, 27000000, 0, 0),
(2, 1, 18, 87000, 0, 0),
(3, 1, 23, 88000, 0, 0),
(4, 2, 25, 27000000, 0, 0),
(5, 2, 18, 87000, 0, 0),
(6, 2, 23, 88000, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `producers`
--

CREATE TABLE `producers` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `producers`
--

INSERT INTO `producers` (`id`, `name`, `deleted`) VALUES
(1, 'China', 0),
(2, 'USA', 0),
(4, 'Japan', 0);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `producer_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` float NOT NULL,
  `sale` float DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `producer_id`, `name`, `description`, `image`, `price`, `sale`, `quantity`, `created_on`, `deleted`) VALUES
(1, 1, 2, 'asd', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1495982233woooa.jpg', 1222, 0, 122, '2017-05-28 15:53:24', 0),
(2, 2, 1, 'Samsung A7', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1495962333download.jpg', 1222220, 0, 100000, '2017-05-28 15:54:48', 0),
(3, 1, 4, 'Samsung S5', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1495982233woooa.jpg', 100000, 12, 20000, '2017-05-28 16:05:33', 0),
(4, 2, 4, 'Iphone', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1495962413download.jpg', 100000, 13, 10000, '2017-05-28 16:06:53', 0),
(5, 1, 4, 'Macbook Pro', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1496025639woooa.jpg', 30000000, 10, 20000, '2017-05-29 09:40:39', 0),
(6, 3, 4, 'Samsung S5', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1495982233woooa.jpg', 100000, 12, 20000, '2017-05-28 16:05:33', 0),
(7, 2, 4, 'Iphone', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1495962413download.jpg', 100000, 13, 10000, '2017-05-28 16:06:53', 0),
(8, 1, 4, 'Macbook Pro', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1496025639woooa.jpg', 30000000, 10, 20000, '2017-05-29 09:40:39', 0),
(9, 3, 4, 'Samsung S5', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1495982233woooa.jpg', 100000, 12, 20000, '2017-05-28 16:05:33', 0),
(10, 2, 4, 'Iphone', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1495962413download.jpg', 100000, 13, 10000, '2017-05-28 16:06:53', 0),
(11, 11, 4, 'Macbook Pro', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1496025639woooa.jpg', 30000000, 10, 20000, '2017-05-29 09:40:39', 0),
(12, 3, 4, 'Samsung S5', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1495982233woooa.jpg', 100000, 12, 20000, '2017-05-28 16:05:33', 0),
(13, 12, 4, 'Iphone', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1495962413download.jpg', 100000, 0, 10000, '2017-05-28 16:06:53', 0),
(14, 13, 4, 'Macbook Pro', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1496025639woooa.jpg', 30000000, 0, 20000, '2017-05-29 09:40:39', 0),
(15, 7, 2, 'asd', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1495962413download.jpg', 1222, 0, 122, '2017-05-28 15:53:24', 0),
(16, 8, 1, 'Samsung A7', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1495962333download.jpg', 1222220, 0, 100000, '2017-05-28 15:54:48', 0),
(17, 12, 4, 'Samsung S5', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1495982233woooa.jpg', 100000, 12, 20000, '2017-05-28 16:05:33', 0),
(18, 10, 4, 'Iphone', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1495962413download.jpg', 100000, 13, 10000, '2017-05-28 16:06:53', 0),
(19, 11, 4, 'Macbook Pro', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1496025639woooa.jpg', 30000000, 10, 20000, '2017-05-29 09:40:39', 0),
(20, 8, 4, 'Samsung S5', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1495982233woooa.jpg', 100000, 12, 20000, '2017-05-28 16:05:33', 0),
(21, 10, 4, 'Iphone', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1495962413download.jpg', 100000, 13, 10000, '2017-05-28 16:06:53', 0),
(22, 13, 4, 'Macbook Pro', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1496025639woooa.jpg', 30000000, 10, 20000, '2017-05-29 09:40:39', 0),
(23, 12, 4, 'Samsung S5', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1495982233woooa.jpg', 100000, 12, 20000, '2017-05-28 16:05:33', 0),
(24, 13, 4, 'Iphone', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1495962413download.jpg', 100000, 13, 10000, '2017-05-28 16:06:53', 0),
(25, 11, 4, 'Macbook Pro', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1496025639woooa.jpg', 30000000, 10, 20000, '2017-05-29 09:40:39', 0),
(26, 9, 4, 'Samsung S5', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1495982233woooa.jpg', 100000, 12, 20000, '2017-05-28 16:05:33', 0),
(27, 2, 4, 'Iphone', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1495962413download.jpg', 100000, 0, 10000, '2017-05-28 16:06:53', 0),
(28, 1, 4, 'Macbook Pro', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'upload/product/1496025639woooa.jpg', 30000000, 0, 20000, '2017-05-29 09:40:39', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sql_log`
--

CREATE TABLE `sql_log` (
  `id` int(11) NOT NULL,
  `ip` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `access_type` tinyint(4) NOT NULL COMMENT '1:web, 2:ios, 3:android',
  `query` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sql_log`
--

INSERT INTO `sql_log` (`id`, `ip`, `access_type`, `query`, `created_on`, `deleted`) VALUES
(17, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''5'' ORDER BY id DESC', '2017-06-11 14:07:42', 0),
(18, '::1', 1, 'SELECT * FROM products WHERE deleted = 0 AND  category_id IN (''13'',''12'',''11'',''10'',''9'',''8'',''7'',''6'') ORDER BY price ASC LIMIT 8', '2017-06-11 14:07:42', 0),
(19, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 14:07:42', 0),
(20, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''1'' ORDER BY id DESC', '2017-06-11 15:19:56', 0),
(21, '::1', 1, 'SELECT p.*, c.name as category_name, pc.name as producer_name\r\n                FROM products as p INNER JOIN categories as c\r\n                ON c.id=p.category_id INNER JOIN producers as pc \r\n                ON pc.id=p.producer_id WHERE c.id=1 OR c.paren', '2017-06-11 15:19:56', 0),
(22, '::1', 1, 'SELECT name,parent_id FROM categories WHERE id = 1 AND deleted =0 LIMIT 1', '2017-06-11 15:19:56', 0),
(23, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 15:19:56', 0),
(24, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''14'' ORDER BY id DESC', '2017-06-11 15:20:37', 0),
(25, '::1', 1, 'SELECT p.*, c.name as category_name, pc.name as producer_name\r\n                FROM products as p INNER JOIN categories as c\r\n                ON c.id=p.category_id INNER JOIN producers as pc \r\n                ON pc.id=p.producer_id WHERE c.id=14 OR c.pare', '2017-06-11 15:20:37', 0),
(26, '::1', 1, 'SELECT name,parent_id FROM categories WHERE id = 14 AND deleted =0 LIMIT 1', '2017-06-11 15:20:37', 0),
(27, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 15:20:37', 0),
(28, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''14'' ORDER BY id DESC', '2017-06-11 16:22:44', 0),
(29, '::1', 1, 'SELECT p.*, c.name as category_name, pc.name as producer_name\r\n                FROM products as p INNER JOIN categories as c\r\n                ON c.id=p.category_id INNER JOIN producers as pc \r\n                ON pc.id=p.producer_id WHERE c.id=14 OR c.pare', '2017-06-11 16:22:45', 0),
(30, '::1', 1, 'SELECT name,parent_id FROM categories WHERE id = 14 AND deleted =0 LIMIT 1', '2017-06-11 16:22:45', 0),
(31, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 16:22:45', 0),
(32, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 16:22:46', 0),
(33, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 16:22:52', 0),
(34, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 16:23:17', 0),
(35, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 16:23:29', 0),
(36, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 16:27:36', 0),
(37, '::1', 1, 'SELECT * FROM users WHERE email = '''' AND deleted =0 LIMIT 1', '2017-06-11 16:35:12', 0),
(38, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 16:35:12', 0),
(39, '::1', 1, 'SELECT * FROM users WHERE email = '''' AND deleted =0 LIMIT 1', '2017-06-11 16:36:02', 0),
(40, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 16:36:02', 0),
(41, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 16:36:17', 0),
(42, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 16:36:31', 0),
(43, '::1', 1, 'SELECT * FROM users WHERE email = ''dasdsa@emsil.com'' AND deleted =0 LIMIT 1', '2017-06-11 16:36:35', 0),
(44, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 16:36:35', 0),
(45, '::1', 1, 'SELECT * FROM users WHERE email = ''dasdsa@emsil.com'' AND deleted =0 LIMIT 1', '2017-06-11 16:37:19', 0),
(46, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 16:37:19', 0),
(47, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 16:41:23', 0),
(48, '::1', 1, 'SELECT * FROM users WHERE email = ''emailtest@email.com'' AND deleted =0 LIMIT 1', '2017-06-11 16:41:32', 0),
(49, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 16:41:57', 0),
(50, '::1', 1, 'SELECT * FROM users WHERE email = ''emailtest@email.com'' AND deleted =0 LIMIT 1', '2017-06-11 16:42:01', 0),
(51, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 16:42:05', 0),
(52, '::1', 1, 'SELECT * FROM users WHERE email = ''anhntgc01047@fpt.edu.vn'' AND deleted =0 LIMIT 1', '2017-06-11 16:44:06', 0),
(53, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 16:44:10', 0),
(54, '::1', 1, 'SELECT * FROM users WHERE deleted = 0 AND email = ''anhntgc01047@fpt.edu.vn'' AND token = ''cedf4cfc3a987a95cbdb1d685fd4a59f'' ORDER BY id DESC', '2017-06-11 17:00:20', 0),
(55, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 17:00:20', 0),
(56, '::1', 1, 'SELECT * FROM users WHERE deleted = 0 AND email = ''anhntgc01047@fpt.edu.vn'' AND token = ''cedf4cfc3a987a95cbdb1d685fd4a59f'' ORDER BY id DESC', '2017-06-11 17:01:07', 0),
(57, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 17:01:07', 0),
(58, '::1', 1, 'SELECT * FROM users WHERE deleted = 0 AND email = ''anhntgc01047@fpt.edu.vn'' AND token = ''cedf4cfc3a987a95cbdb1d685fd4a59f'' ORDER BY id DESC', '2017-06-11 17:01:25', 0),
(59, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 17:01:25', 0),
(60, '::1', 1, 'SELECT * FROM users WHERE deleted = 0 AND email = ''anhntgc01047@fpt.edu.vn'' AND token = ''cedf4cfc3a987a95cbdb1d685fd4a59f'' ORDER BY id DESC', '2017-06-11 17:01:54', 0),
(61, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 17:01:54', 0),
(62, '::1', 1, 'SELECT * FROM users WHERE deleted = 0 AND email = ''anhntgc01047@fpt.edu.vn'' AND token = ''cedf4cfc3a987a95cbdb1d685fd4a59f'' ORDER BY id DESC', '2017-06-11 17:02:03', 0),
(63, '::1', 1, 'SELECT * FROM users WHERE deleted = 0 AND email = ''anhntgc01047@fpt.edu.vn'' AND token = ''cedf4cfc3a987a95cbdb1d685fd4a59f'' ORDER BY id DESC', '2017-06-11 17:14:57', 0),
(64, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 17:14:57', 0),
(65, '::1', 1, 'SELECT * FROM users WHERE deleted = 0 AND email = ''anhntgc01047@fpt.edu.vn'' AND token = ''cedf4cfc3a987a95cbdb1d685fd4a59f'' ORDER BY id DESC', '2017-06-11 17:15:03', 0),
(66, '::1', 1, 'UPDATE users SET password=''nguyen4nh'' WHERE id=8', '2017-06-11 17:15:03', 0),
(67, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 17:15:03', 0),
(68, '::1', 1, 'SELECT * FROM users WHERE deleted = 0 AND email = ''anhntgc01047@fpt.edu.vn'' AND token = ''cedf4cfc3a987a95cbdb1d685fd4a59f'' ORDER BY id DESC', '2017-06-11 17:15:46', 0),
(69, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 17:15:46', 0),
(70, '::1', 1, 'SELECT * FROM users WHERE deleted = 0 AND email = ''anhntgc01047@fpt.edu.vn'' AND token = ''cedf4cfc3a987a95cbdb1d685fd4a59f'' ORDER BY id DESC', '2017-06-11 17:15:50', 0),
(71, '::1', 1, 'UPDATE users SET password=''nguyen4nh'' WHERE id=8', '2017-06-11 17:15:50', 0),
(72, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 17:15:50', 0),
(73, '::1', 1, 'SELECT * FROM users WHERE deleted = 0 AND email = ''anhntgc01047@fpt.edu.vn'' AND token = ''cedf4cfc3a987a95cbdb1d685fd4a59f'' ORDER BY id DESC', '2017-06-11 17:17:49', 0),
(74, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 17:17:49', 0),
(75, '::1', 1, 'SELECT * FROM users WHERE deleted = 0 AND email = ''anhntgc01047@fpt.edu.vn'' AND token = ''cedf4cfc3a987a95cbdb1d685fd4a59f'' ORDER BY id DESC', '2017-06-11 17:18:01', 0),
(76, '::1', 1, 'UPDATE users SET password=''nguyen4nh'' WHERE id=8', '2017-06-11 17:18:01', 0),
(77, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 17:18:01', 0),
(78, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 17:18:01', 0),
(79, '::1', 1, 'SELECT u.id,u.name,u.email,u.address,u.phone,\r\n                g.name as group_name , g.id as group_id,\r\n                ug.id as user_group_id\r\n                FROM users as u INNER JOIN user_group as ug\r\n                ON u.id=ug.user_id INNER JOIN gro', '2017-06-11 17:18:14', 0),
(80, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 17:18:14', 0),
(81, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 17:20:13', 0),
(82, '::1', 1, 'SELECT * FROM users WHERE deleted = 0 AND email = ''anhntgc01047@fpt.edu.vn'' AND token = ''cedf4cfc3a987a95cbdb1d685fd4a59f'' ORDER BY id DESC', '2017-06-11 17:20:14', 0),
(83, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 17:20:14', 0),
(84, '::1', 1, 'SELECT * FROM users WHERE deleted = 0 AND email = ''anhntgc01047@fpt.edu.vn'' AND token = ''cedf4cfc3a987a95cbdb1d685fd4a59f'' ORDER BY id DESC', '2017-06-11 17:20:17', 0),
(85, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 17:20:17', 0),
(86, '::1', 1, 'SELECT * FROM users WHERE deleted = 0 AND email = ''anhntgc01047@fpt.edu.vn'' AND token = ''cedf4cfc3a987a95cbdb1d685fd4a59f'' ORDER BY id DESC', '2017-06-11 17:20:21', 0),
(87, '::1', 1, 'UPDATE users SET password=''6d20d52e49f6d7b62e3bb6a5203adc4d'',token=''d8ec75ca9438eb0e0b67affb2df4211e'' WHERE id=8', '2017-06-11 17:20:21', 0),
(88, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 17:20:21', 0),
(89, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 17:20:21', 0),
(90, '::1', 1, 'SELECT u.id,u.name,u.email,u.address,u.phone,\r\n                g.name as group_name , g.id as group_id,\r\n                ug.id as user_group_id\r\n                FROM users as u INNER JOIN user_group as ug\r\n                ON u.id=ug.user_id INNER JOIN gro', '2017-06-11 17:20:29', 0),
(91, '::1', 1, 'INSERT INTO login_log(ip,access_type,user_id,login_result) VALUES (''::1'', ''1'', ''8'', ''1'')', '2017-06-11 17:20:29', 0),
(92, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 17:20:29', 0),
(93, '::1', 1, 'SELECT p.*, c.name as category_name, pc.name as producer_name\r\n                FROM products as p INNER JOIN categories as c\r\n                ON c.id=p.category_id INNER JOIN producers as pc \r\n                ON pc.id=p.producer_id WHERE p.deleted = 0 AND', '2017-06-11 17:20:29', 0),
(94, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''5'' ORDER BY id DESC', '2017-06-11 17:20:29', 0),
(95, '::1', 1, 'SELECT * FROM products WHERE deleted = 0 AND  category_id IN (''13'',''12'',''11'',''10'',''9'',''8'',''7'',''6'') ORDER BY price ASC LIMIT 8', '2017-06-11 17:20:29', 0),
(96, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 17:20:29', 0),
(97, '::1', 1, 'SELECT p.*, c.name as category_name, pc.name as producer_name\r\n                FROM products as p INNER JOIN categories as c\r\n                ON c.id=p.category_id INNER JOIN producers as pc \r\n                ON pc.id=p.producer_id WHERE p.deleted = 0 AND', '2017-06-11 17:20:38', 0),
(98, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''5'' ORDER BY id DESC', '2017-06-11 17:20:38', 0),
(99, '::1', 1, 'SELECT * FROM products WHERE deleted = 0 AND  category_id IN (''13'',''12'',''11'',''10'',''9'',''8'',''7'',''6'') ORDER BY price ASC LIMIT 8', '2017-06-11 17:20:38', 0),
(100, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-11 17:20:38', 0),
(101, '::1', 1, 'SELECT p.*, c.name as category_name, pc.name as producer_name\r\n                FROM products as p INNER JOIN categories as c\r\n                ON c.id=p.category_id INNER JOIN producers as pc \r\n                ON pc.id=p.producer_id WHERE p.deleted = 0 AND', '2017-06-12 01:04:21', 0),
(102, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''5'' ORDER BY id DESC', '2017-06-12 01:04:21', 0),
(103, '::1', 1, 'SELECT * FROM products WHERE deleted = 0 AND  category_id IN (''13'',''12'',''11'',''10'',''9'',''8'',''7'',''6'') ORDER BY price ASC LIMIT 8', '2017-06-12 01:04:21', 0),
(104, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-12 01:04:21', 0),
(105, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-12 01:04:23', 0),
(106, '::1', 1, 'SELECT u.id,u.name,u.email,u.address,u.phone,\r\n                g.name as group_name , g.id as group_id,\r\n                ug.id as user_group_id\r\n                FROM users as u INNER JOIN user_group as ug\r\n                ON u.id=ug.user_id INNER JOIN gro', '2017-06-12 01:04:35', 0),
(107, '::1', 1, 'INSERT INTO login_log(ip,access_type,user_id,login_result) VALUES (''::1'', ''1'', ''8'', ''1'')', '2017-06-12 01:04:35', 0),
(108, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-12 01:04:35', 0),
(109, '::1', 1, 'SELECT p.*, c.name as category_name, pc.name as producer_name\r\n                FROM products as p INNER JOIN categories as c\r\n                ON c.id=p.category_id INNER JOIN producers as pc \r\n                ON pc.id=p.producer_id WHERE p.deleted = 0 AND', '2017-06-12 01:04:35', 0),
(110, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''5'' ORDER BY id DESC', '2017-06-12 01:04:35', 0),
(111, '::1', 1, 'SELECT * FROM products WHERE deleted = 0 AND  category_id IN (''13'',''12'',''11'',''10'',''9'',''8'',''7'',''6'') ORDER BY price ASC LIMIT 8', '2017-06-12 01:04:35', 0),
(112, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-12 01:04:35', 0),
(113, '::1', 1, 'SELECT p.*, c.name as category_name, pc.name as producer_name\r\n                FROM products as p INNER JOIN categories as c\r\n                ON c.id=p.category_id INNER JOIN producers as pc \r\n                ON pc.id=p.producer_id WHERE p.deleted = 0 AND', '2017-06-12 01:05:08', 0),
(114, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''5'' ORDER BY id DESC', '2017-06-12 01:05:09', 0),
(115, '::1', 1, 'SELECT * FROM products WHERE deleted = 0 AND  category_id IN (''13'',''12'',''11'',''10'',''9'',''8'',''7'',''6'') ORDER BY price ASC LIMIT 8', '2017-06-12 01:05:09', 0),
(116, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-12 01:05:09', 0),
(117, '::1', 1, 'SELECT p.*, c.name as category_name, pc.name as producer_name\r\n                FROM products as p INNER JOIN categories as c\r\n                ON c.id=p.category_id INNER JOIN producers as pc \r\n                ON pc.id=p.producer_id WHERE p.deleted = 0 AND', '2017-06-12 01:07:50', 0),
(118, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''5'' ORDER BY id DESC', '2017-06-12 01:07:50', 0),
(119, '::1', 1, 'SELECT * FROM products WHERE deleted = 0 AND  category_id IN (''13'',''12'',''11'',''10'',''9'',''8'',''7'',''6'') ORDER BY price ASC LIMIT 8', '2017-06-12 01:07:50', 0),
(120, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-12 01:07:50', 0),
(121, '::1', 1, 'SELECT o.*,od.*,p.name as product_name ,p.image as image \r\n                FROM orders as o \r\n                JOIN order_detail as od \r\n                ON o.id=od.order_id \r\n                JOIN products as p ON p.id = od.product_id \r\n                WHER', '2017-06-12 01:07:53', 0),
(122, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-12 01:07:53', 0),
(123, '::1', 1, 'SELECT o.*,od.*,p.name as product_name ,p.image as image \r\n                FROM orders as o \r\n                JOIN order_detail as od \r\n                ON o.id=od.order_id \r\n                JOIN products as p ON p.id = od.product_id \r\n                WHER', '2017-06-12 01:08:51', 0),
(124, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-12 01:08:51', 0),
(125, '::1', 1, 'SELECT o.*,od.*,p.name as product_name ,p.image as image \r\n                FROM orders as o \r\n                JOIN order_detail as od \r\n                ON o.id=od.order_id \r\n                JOIN products as p ON p.id = od.product_id \r\n                WHER', '2017-06-12 01:08:58', 0),
(126, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-12 01:08:58', 0),
(127, '::1', 1, 'SELECT o.*,od.*,p.name as product_name ,p.image as image \r\n                FROM orders as o \r\n                JOIN order_detail as od \r\n                ON o.id=od.order_id \r\n                JOIN products as p ON p.id = od.product_id \r\n                WHER', '2017-06-12 01:11:09', 0),
(128, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-12 01:11:09', 0),
(129, '::1', 1, 'SELECT p.*, c.name as category_name, pc.name as producer_name\r\n                FROM products as p INNER JOIN categories as c\r\n                ON c.id=p.category_id INNER JOIN producers as pc \r\n                ON pc.id=p.producer_id WHERE p.deleted = 0 AND', '2017-06-12 01:11:14', 0),
(130, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''5'' ORDER BY id DESC', '2017-06-12 01:11:14', 0),
(131, '::1', 1, 'SELECT * FROM products WHERE deleted = 0 AND  category_id IN (''13'',''12'',''11'',''10'',''9'',''8'',''7'',''6'') ORDER BY price ASC LIMIT 8', '2017-06-12 01:11:14', 0),
(132, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-12 01:11:14', 0),
(133, '::1', 1, 'SELECT p.*, c.name as category_name, pc.name as producer_name\r\n                FROM products as p INNER JOIN categories as c\r\n                ON c.id=p.category_id INNER JOIN producers as pc \r\n                ON pc.id=p.producer_id WHERE p.id = 27 AND p.d', '2017-06-12 01:11:16', 0),
(134, '::1', 1, 'SELECT * FROM products WHERE deleted = 0 AND category_id = ''2'' ORDER BY id DESC LIMIT 5', '2017-06-12 01:11:16', 0),
(135, '::1', 1, 'SELECT * FROM comments WHERE deleted = 0 AND product_id = ''27'' ORDER BY created_on ASC LIMIT 3', '2017-06-12 01:11:16', 0),
(136, '::1', 1, 'SELECT count(id) as count FROM comments WHERE deleted=0 AND product_id = 27', '2017-06-12 01:11:16', 0),
(137, '::1', 1, 'SELECT * FROM categories WHERE deleted = 0 AND parent_id = ''0'' ORDER BY id DESC', '2017-06-12 01:11:16', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notification` tinyint(1) NOT NULL DEFAULT '0',
  `token` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `phone`, `address`, `notification`, `token`, `deleted`) VALUES
(1, 'admin@admin.com', 'aed2c3110bbcdd91a987d8b1ab87fba6', 'Admin', '12345675', '', 0, '12345678', 0),
(2, 'anhnt@email.com', '6d20d52e49f6d7b62e3bb6a5203adc4d', 'Nguyen The Anh', '', '', 0, '2fa53cb276db998cbd6f63f1f92a2513', 0),
(3, 'anhnt@email.com', 'df7e585bbc5a33bbec94feefc197b770', 'Anhnt0906', '0987654321', 'HCM', 0, '2fa53cb276db998c236f63f1f92a251e', 0),
(4, 'nguyena@email.com', '0acd758b911c6744e2a0db8077bd82d5', 'Nguyen A', '12345678912', 'HN', 0, '2f783cb276db998cbd6f63f1f92a251e', 0),
(6, 'theanhhn1@gmail.com', '6d20d52e49f6d7b62e3bb6a5203adc4d', 'Nguyen The Anh', '12345679787', 'Dong Anh', 0, '2fa53cb276db998cbd6f63f1f92a251e', 0),
(7, 'nguyena@email.com', 'e277dd1e05688a22e377e25a3dae5de1', 'Nguyen Van A', '543657653435', 'Ha Noi', 1, '1e5e870f6d85c0f4e2bd9297026294e0', 0),
(8, 'anhntgc01047@fpt.edu.vn', '6d20d52e49f6d7b62e3bb6a5203adc4d', 'Nguyen The Anh', '12345689', 'HN', 1, 'd8ec75ca9438eb0e0b67affb2df4211e', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`id`, `user_id`, `group_id`, `deleted`) VALUES
(1, 1, 1, 0),
(2, 3, 2, 0),
(3, 4, 2, 0),
(4, 6, 3, 0),
(5, 7, 3, 0),
(6, 8, 3, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_log`
--
ALTER TABLE `login_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `producers`
--
ALTER TABLE `producers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sql_log`
--
ALTER TABLE `sql_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `login_log`
--
ALTER TABLE `login_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `producers`
--
ALTER TABLE `producers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `sql_log`
--
ALTER TABLE `sql_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
