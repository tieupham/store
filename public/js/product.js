$(document).ready(function () {
    $(document).on("click", ".e_add_to_cart", add_cart);
    $(document).on("click", ".pagination span", load_more_comment);
    $('.pagination li').first().addClass("active");

    function add_cart(e) {
        var product_id = $(this).attr("data-id");
        var url = $(this).attr("data-url");
        $.ajax({
            url     : url,
            type    : "POST",
            data    : {'id': product_id},
            dataType: "json",
            success : function (data) {
                var current_count = $('.count-cart-item').html();
                alert(data['message']);
                if (data['status']) {
                    current_count++;
                    $('.count-cart-item').html(current_count);
                }
            },
            error   : function (a, b, c) {
                console.log(a, b, c);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    }

    function load_more_comment(e) {
        var ob = $('nav.e-load-more');
        var url = ob.attr("data-url");
        var product_id = ob.attr("data-product-id");
        var page = $(this).attr('data-page');
        var span = $(this);
        $.ajax({
            url     : url,
            type    : "POST",
            data    : {
                'page'      : page,
                'product_id': product_id
            },
            dataType: "json",
            success : function (data) {
                if (data['status']) {
                    $('.pagination li').removeClass('active');
                    span.parent().addClass('active');
                    $('.comment-content').html(data['data']);
                }
            }

            ,
            error   : function (a, b, c) {
                console.log(a, b, c);
            }
            ,
            complete: function (jqXHR, textStatus) {
            }
        })
        ;
    }
});