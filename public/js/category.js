$(document).ready(function () {
    $('.e-load-more').attr('data-page', 2);
    $(document).on('click', '.e-load-more', load_more_product);

    function load_more_product(e) {
        var cat_id = $(this).attr('data-category');
        var current_page = $(this).attr('data-page');
        var url = $(this).attr('data-url');
        $.ajax({
            url     : url,
            type    : "POST",
            data    : {'category_id': cat_id, 'page': current_page},
            dataType: "json",
            success : function (data) {
                if (data['status']) {
                    var html = "";
                    $('.e-load-more').attr('data-page',  parseInt(current_page) + 1);
                    $.each(data['products'], function (index, value) {
                        html += "<a href='" + value['url'] + "product/index/" + value['id'] + "'> " +
                            "<div class='col-sm-2-4 product-thumb'> " +
                            "<div class='thumbnail'> " +
                            "<img class='img-responsive' src='" + value['url'] + value['image'] + "'" +
                            "alt='" + value['name'] + "' width='50%'> " +
                            "<div class='caption'> " +
                            "<h4>" + value['name'] + "</h4> " +
                            "<strong>" + value['price'] + " đ</strong> " +
                            "<p class= 'old-price' >" + value['old_price'] + "</p> </div > </div > </div > </a> ";
                    });
                    $('.data-load-more').append(html);
                }else{
                    $('.e-load-more').click(false);
                }
            },
            error   : function (a, b, c) {
                console.log(a, b, c);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    }
});
