$(document).ready(function () {
    $(document).on("click", "button.e-update-cart", update_cart);
    $(document).on("click", "span.e-delete-product", delete_product);
    $(document).on("click", "span.e-cancel-order", cancel_order);

    function update_cart(e) {
        var product_id = $(this).attr("data-id");
        var new_count = $('input.data-count-' + product_id).val();
        var url = $('.e-data-url').attr("data-url-update");
        $.ajax({
            url     : url,
            type    : "POST",
            data    : {'key': product_id, 'new_count': new_count},
            dataType: "json",
            success : function (data) {
                alert(data['message']);
                if (data['status']) {
                    location.reload();
                } else {
                    console.log(data['message']);
                }
            },
            error   : function (a, b, c) {
                console.log(a, b, c);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    }

    function delete_product(e) {
        var url = $('.e-data-url').attr("data-url-delete");
        var product_id = $(this).attr("data-id");
        $.ajax({
            url     : url,
            type    : "POST",
            data    : {'key': product_id},
            dataType: "json",
            success : function (data) {
                alert(data['message']);
                if (data['status']) {
                    location.reload();
                } else {
                    console.log(data['message']);
                }

            },
            error   : function (a, b, c) {
                console.log(a, b, c);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    }

    function cancel_order(e) {
        if (confirm("Bạn có muốn hủy đơn hàng này không?")) {
            var url = $('.__history_order').attr('data-ajax-url');
            var order_id = $(this).attr('data-order-id');

            $.ajax({
                url     : url,
                type    : "POST",
                data    : {'order_id': order_id},
                dataType: "json",
                success : function (data) {
                    if (data['status']) {
                        location.reload();
                    }else {
                        console.log('xxxx');
                    }
                },
                error   : function (a, b, c) {
                    console.log(a, b, c);
                },
                complete: function (jqXHR, textStatus) {
                }
            });
        }
        e.preventDefault();
    }
});