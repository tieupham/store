<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Product
                    <small>Add</small>
                </h1>
                <?php if (!empty($data['errors'])) { ?>
                    <div class="alert alert-info">
                        <ul>
                            <?php foreach ($data['errors'] as $e) { ?>
                                <li><?php echo $e; ?></li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                <form action="" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Category name</label>
                        <select class="form-control" name="category_id">
                            <?php if ($data['categories']) {
                                foreach ($data['categories'] as $cat) { ?>
                                    <option value="<?php echo $cat['id'] ?>" ><?php echo $cat['name']; ?></option>
                                <?php }
                            } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Producer</label>
                        <select class="form-control" name="producer_id">
                            <?php if ($data['producers']) {
                                foreach ($data['producers'] as $producer) { ?>
                                    <option value="<?php echo $producer['id'] ?>" ><?php echo $producer['name']; ?></option>
                                <?php }
                            } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Product name</label>
                        <input class="form-control" name="name"/>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" name="description"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Image</label>
                        <input type="file" class="form-control" name="image"/>
                    </div>
                    <div class="form-group">
                        <label>Price</label>
                        <input type="number" class="form-control" name="price"/>
                    </div>
                    <div class="form-group">
                        <label>Sale(%)</label>
                        <input type="number" class="form-control" name="sale" value="0"/>
                    </div>
                    <div class="form-group">
                        <label>Quantity</label>
                        <input type="number" class="form-control" name="quantity"/>
                    </div>
                    <button type="submit" class="btn btn-default" name="submit">Product Add</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                    <form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
