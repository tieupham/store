<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Product
                    <small>List</small>
                </h1>
                <?php if (isset($_SESSION['msg'])) { ?>
                    <div class="alert alert-info"><?php echo $_SESSION['msg'] ?></div>
                    <?php unset($_SESSION['msg']);
                } ?>
            </div>
            <!-- /.col-lg-12 -->
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                <tr align="center">
                    <th>STT</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Producer</th>
                    <th>Description</th>
                    <th>Image</th>
                    <th>Price</th>
                    <th>Sale</th>
                    <th>Quantity</th>
                    <th>Date</th>
                    <th>Delete</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($data['products'])){
                $i = 1;
                foreach ($data['products'] as $product){
                ?>
                <tr class="odd gradeX" align="center">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $product['name'] ?></td>
                    <td><?php echo $product['category_name'] ?></td>
                    <td><?php echo $product['producer_name'] ?></td>
                    <td><?php echo $product['description'] ?></td>
                    <td><img src="<?php echo base_url . $product['image'] ?>" style="height:50px;"></td>
                    <td><?php echo $product['price'] ?></td>
                    <td><?php echo $product['sale'] ?></td>
                    <td><?php echo $product['quantity'] ?></td>
                    <td><?php echo date('d-m-Y', strtotime($product['created_on'])) ?></td>
                    <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a
                                href="<?php echo base_url . 'admin/product/delete/' . $product['id'] ?>"
                                onclick="return confirm('Are you sure?')"> Delete</a></td>
                    <td class="center"><i class="fa fa-pencil fa-fw"></i> <a
                                href="<?php echo base_url . 'admin/product/edit/' . $product['id'] ?>">Edit</a></td>
                </tr>
                </tbody>
                <?php
                $i++;
                }
                } ?>
            </table>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->