<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Order
                    <small>List</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <table class="table table-striped table-bordered table-hover table-responsive" id="dataTables-example"
                   data-url ="<?php echo base_url.'admin/order/confirm_order'?>">
                <thead>
                <tr align="center">
                    <th>STT</th>
                    <th>Code</th>
                    <th>Customer name</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Total price</th>
                    <th>Date</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($data['list_order'])){
                $i = 1;
                foreach ($data['list_order'] as $order){
                ?>
                <tr class="odd gradeX" align="center">
                    <td><?php echo $i; ?></td>
                    <td>
                        <a href="<?php echo base_url . 'admin/order/detail/' . $order['id'] ?>"><?php echo $order['order_code']; ?></a>
                    </td>
                    <td><?php echo $order['user_name'] ?></td>
                    <td><?php echo $order['address'] ?></td>
                    <td><?php echo $order['phone'] ?></td>
                    <td><?php echo number_format($order['total_price']) . ' đ' ?></td>
                    <td><?php echo $order['created_on'] ?></td>
                    <td>
                        <?php if ($order['status'] == 0) { ?>
                            <span class="btn btn-primary e-confirm-order-trans" data-order="<?php echo $order['id'] ?>">Xác nhận giao hàng</span>
                        <?php } else {
                            echo "Đã giao hàng";
                        } ?>
                    </td>
                </tr>
                </tbody>
                <?php
                $i++;
                }
                } ?>
            </table>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->