<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Order
                    <small>
                        Order detail- <?php if (!empty($data['order_details'])) echo $data['order_details'][0]['order_code'] ?>
                    </small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                <tr align="center">
                    <th>STT</th>
                    <th>Product name</th>
                    <th>Image</th>
                    <th>Price</th>
                    <th>Count</th>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($data['order_details'])){
                $i = 1;
                foreach ($data['order_details'] as $order){
                ?>
                <tr class="odd gradeX" align="center">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $order['product_name'] ?></td>
                    <td><img src="<?php echo base_url . $order['image'] ?>" alt="" height="100px"></td>
                    <td><?php echo $order['price'] ?></td>
                    <td><?php echo $order['count'] ?></td>
                </tr>
                </tbody>
                <?php
                $i++;
                }
                } ?>
            </table>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->