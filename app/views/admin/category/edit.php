<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Category
                    <small>Edit</small>
                </h1>
                <?php if (!empty($data['errors'])) { ?>
                    <div class="alert alert-info">
                        <ul>
                            <?php foreach ($data['errors'] as $e) { ?>
                                <li><?php echo $e; ?></li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                <?php if (isset($data['category'])){ ?>
                <form action="" method="POST">
                    <div class="form-group">
                        <label>Category Parent</label>
                        <select class="form-control" name="parent_id">
                            <option value="0">Please Choose Category</option>
                            <?php if ($data['list_categories']) {
                                foreach ($data['list_categories'] as $cat) { ?>
                                    <option value="<?php echo $cat['id'] ?>"
                                    <?php  echo ($cat['id'] ==  $data['category']['parent_id'])? 'selected' :''; ?>>
                                        <?php echo $cat['name']; ?>
                                    </option>
                                <?php }
                            } ?>\
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Category Name</label>
                        <input class="form-control" name="name" value="<?php echo $data['category']['name'] ?>"/>
                    </div>
                    <button type="submit" class="btn btn-default" name="submit">Save Edit</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                    <form>
                        <?php
                        } else { ?>
                            <h1>This category is not exist!</h1>
                        <?php } ?>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
