<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Producer
                    <small>Add</small>
                </h1>
                <?php if (!empty($data['errors'])) { ?>
                    <div class="alert alert-info">
                        <ul>
                            <?php foreach ($data['errors'] as $e) { ?>
                                <li><?php echo $e; ?></li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                <form action="" method="POST">
                    <div class="form-group">
                        <label>Producer Name</label>
                        <input class="form-control" name="name" placeholder="Please Enter Category Name"/>
                    </div>
                    <button type="submit" class="btn btn-default" name="submit">Producer Add</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                    <form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
