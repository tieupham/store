<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Producer
                    <small>List</small>
                </h1>
                <?php if (isset($_SESSION['msg'])) { ?>
                            <div class="alert alert-info"><?php echo $_SESSION['msg']?></div>
                <?php unset($_SESSION['msg']); } ?>
            </div>
            <!-- /.col-lg-12 -->
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                <tr align="center">
                    <th>STT</th>
                    <th>Name</th>
                    <th>Delete</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($data['producers'])){
                $i = 1;
                foreach ($data['producers'] as $producer){
                ?>
                <tr class="odd gradeX" align="center">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $producer['name'] ?></td>
                    <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a
                                href="<?php echo base_url . 'admin/producer/delete/' . $producer['id'] ?>" onclick="return confirm('Are you sure?')"> Delete</a></td>
                    <td class="center"><i class="fa fa-pencil fa-fw"></i> <a
                                href="<?php echo base_url . 'admin/producer/edit/' . $producer['id'] ?>">Edit</a></td>
                </tr>
                </tbody>
                <?php
                $i++;
                }
                } ?>
            </table>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->