<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">User
                    <small>Add</small>
                </h1>
                <?php if (!empty($data['errors'])) { ?>
                    <div class="alert alert-info">
                        <ul>
                            <?php foreach ($data['errors'] as $e) { ?>
                                <li><?php echo $e; ?></li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                <form action="" method="POST">
                    <div class="form-group">
                        <label>Type of user</label>
                        <select class="form-control" name="group_id">
                            <?php if ($data['groups']) {
                                foreach ($data['groups'] as $group) { ?>
                                    <option value="<?php echo $group['id'] ?>" <?php if($group['id']==1) echo "disabled"; ?> ><?php echo $group['name']; ?></option>
                                <?php }
                            } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Full name</label>
                        <input class="form-control" name="name"/>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input class="form-control" name="email"/>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password"/>
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <input class="form-control" name="address"/>
                    </div>
                    <div class="form-group">
                        <label>Phone number</label>
                        <input type="number" class="form-control" name="phone"/>
                    </div>
                    <button type="submit" class="btn btn-default" name="submit">User Add</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                    <form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
