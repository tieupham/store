<nav class="navbar navbar-default nav-style">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url . 'home' ?>"><b>STORE</b></a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php if (isset($data['menu'])) {
                    foreach ($data['menu'] as $item) {
                        ?>
                        <li>
                            <a href="<?php echo base_url . 'category/index/' . $item['id'] ?>"><b
                                        class="text-uppercase"><?php echo $item['name'] ?></b></a>
                        </li>
                        <?php
                    }
                } ?>
                <li><a href="<?php echo base_url . 'home' ?>"><b>TIN TỨC</b></a></li>
            </ul>
            <form action="<?php echo base_url . 'product/search' ?>" method="POST" class="navbar-form navbar-left">
                <div class="form-group">
                    <input type="text" class="form-control" name="key_word" placeholder="Search">
                </div>
                <button type="submit" name="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i>
                </button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <?php if (isset($_SESSION['customer'])) { ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false"><span
                                    class="glyphicon glyphicon-user"></span><b>
                                <?php echo $_SESSION['customer']['name'] ?></b> <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url . 'user/logout' ?>">LOGOUT</a></li>
                            <li><a href="<?php echo base_url.'cart/trans_history'?>">LỊCH SỬ GD</a></li>
                        </ul>
                    </li>
                <?php } else { ?>
                    <li><a href="<?php echo base_url . 'user/login' ?>"><span class="glyphicon glyphicon-log-in"></span><b>
                                login</b></a></li>
                <?php } ?>
                <li><a href="<?php echo base_url . 'cart/' ?>"><i
                                class="glyphicon glyphicon-shopping-cart"></i><span
                                class="label label-info count-cart-item"><?php if (isset($_SESSION['cart'])) echo count($_SESSION['cart']) ?></span>
                    </a></li>

            </ul>
        </div>
    </div>
</nav>
<div class="container content">