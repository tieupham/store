<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Admin - Store">
    <meta name="author" content="">
    <title>Store</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url ?>admin_asset/bower_components/bootstrap/dist/css/bootstrap.min.css"
          rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url ?>admin_asset/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url ?>admin_asset/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url ?>admin_asset/bower_components/font-awesome/css/font-awesome.min.css"
          rel="stylesheet" type="text/css">

    <!-- DataTables CSS -->
    <link href="<?php echo base_url ?>admin_asset/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css"
          rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_url ?>admin_asset/bower_components/datatables-responsive/css/dataTables.responsive.css"
          rel="stylesheet">

    <link href="<?php echo base_url ?>css/style.css" rel="stylesheet">
    <?php if (isset($data['css']) && !empty($data['css'])) {
        foreach ($data['css'] as $cs) { ?>
            <link href="<?php echo base_url . $cs ?>" rel="stylesheet">
        <?php }
    } ?>

    <!-- jQuery -->
    <script src="<?php echo base_url ?>admin_asset/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url ?>admin_asset/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url ?>admin_asset/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url ?>admin_asset/dist/js/sb-admin-2.js"></script>
    <?php if (isset($data['js']) && !empty($data['js'])) {
        foreach ($data['js'] as $js) { ?>
            <script src="<?php echo base_url . $js ?>"></script>
        <?php }
    } ?>
    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url ?>admin_asset/bower_components/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url ?>admin_asset/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });
    </script>
</head>

<body>
