<?php if (!empty($data['product'])) {
    $product = $data['product'];
    if(isset($data['msg'])){?>
        <div class="alert alert-info">
            <p><?php echo $data['msg'] ?></p>
        </div>
    <?php }
    ?>
    <div class="page-header">
        <h1><?php echo $product['name'] ?></h1>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <image class="img-responsive center-block" src="<?php echo base_url . $product['image'] ?>" width="80%"/>
        </div>
        <div class="col-sm-6">
            <div class="col-sm-6 center-block">
                <div class="product-status">
                    <h5>Giá sản phẩm</h5>
                    <?php if ($product['sale'] > 0) {
                        $price = $product['price'] - ($product['price'] * $product['sale']) / 100;
                        $o_price = $product['price'];
                    } else {
                        $price = $product['price'];
                    } ?>
                    <strong><?php echo number_format($price); ?><sup> đ</sup></strong>
                    <?php if (isset($o_price)) echo "<span>" . number_format($o_price) . "<sup> đ</sup></span>" ?>
                    <h5>Nhà sản xuất</h5>
                    <b><?php echo $product['producer_name'] ?></b>
                    <h5>Tình trạng hàng</h5>
                    <strong>
                        <?php echo $product['quantity'] > 0 ? "Còn hàng" : "Tạm thời hết hàng" ?>
                    </strong>
                </div>
                <div class="btn-product">
                    <button class=" text-center btn btn-primary btn-block" role="button">Mua hàng</button>
                    <button class="text-center btn btn-primary btn-block e_add_to_cart" role="button"
                            data-url="<?php echo base_url . 'cart/add_cart' ?>"
                            data-id="<?php echo $product['id'] ?>">Thêm vào giỏ hàng
                    </button>
                </div>
            </div>
            <div class="col-sm-6">
                //làm gì đó
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-8">
            <div class="page-header">
                <h4>MÔ TẢ SẢN PHẨM</h4>
            </div>
            <p><?php echo $product['description'] ?></p>
        </div>
        <div class="col-md-4">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">TIN TỨC</h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <image class="img-responsive center-block"
                                   src="<?php echo base_url . 'upload/product/1496025639woooa.jpg' ?>" width="95%"/>
                        </div>
                        <div class="col-sm-8">
                            <p>"Không có ai muốn khổ đau cho chính mình, muốn tìm kiếm về nó và muốn có nó, bởi vì nó là
                                sự
                                đau khổ..."</p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-4">
                            <image class="img-responsive center-block"
                                   src="<?php echo base_url . 'upload/product/1496025639woooa.jpg' ?>" width="95%"/>
                        </div>
                        <div class="col-sm-8">
                            <p>"Không có ai muốn khổ đau cho chính mình, muốn tìm kiếm về nó và muốn có nó, bởi vì nó là
                                sự
                                đau khổ..."</p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-4">
                            <image class="img-responsive center-block"
                                   src="<?php echo base_url . 'upload/product/1496025639woooa.jpg' ?>" width="95%"/>
                        </div>
                        <div class="col-sm-8">
                            <p>"Không có ai muốn khổ đau cho chính mình, muốn tìm kiếm về nó và muốn có nó, bởi vì nó là
                                sự
                                đau khổ..."</p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-4">
                            <image class="img-responsive center-block"
                                   src="<?php echo base_url . 'upload/product/1496025639woooa.jpg' ?>" width="95%"/>
                        </div>
                        <div class="col-sm-8">
                            <p>"Không có ai muốn khổ đau cho chính mình, muốn tìm kiếm về nó và muốn có nó, bởi vì nó là
                                sự
                                đau khổ..."</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="panel panel-info">
            <div class="panel-heading"><h4 class="panel-title">SẢN PHẨM LIÊN QUAN</h4></div>
            <div class="panel-body">
                <?php if (isset($data['product_involve']) && !empty($data['product_involve'])) {
                    foreach ($data['product_involve'] as $item) { ?>
                        <div class="col-sm-2-4">
                            <div class="thumbnail">
                                <img class="img-responsive" src="<?php echo base_url . $item['image'] ?>"
                                     alt="<?php echo $item['name'] ?>" width="65%" height="40%">
                                <div class="caption text-center">
                                    <h5><?php echo $item['name'] ?></h5>
                                    <?php if ($item['sale'] > 0) {
                                        $price = $item['price'] - ($item['price'] * $item['sale']) / 100;
                                        $o_price = $item['price'];
                                    } else {
                                        $price = $item['price'];
                                    } ?>
                                    <strong><?php echo number_format($price) . ' đ'; ?></strong>
                                    <h6><?php if (isset($o_price)) echo number_format($o_price) . ' đ'; ?></h6>
                                </div>
                            </div>
                        </div>
                    <?php }
                } ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h4 class="panel-title">BÌNH LUẬN</h4>
            </div>
            <div class="panel-body">
                <div class="comment-content">
                    <?php
                    if (!empty($data['comments'])) {
                        foreach ($data['comments'] as $comment) { ?>
                            <div class="media">
                                <div class="media-left media-object">
                                    <h4><?php echo $comment['user_name'] ?></h4>
                                    <p><?php echo date('d/m/Y', strtotime($comment['created_on'])) ?></p>
                                </div>
                                <div class="media-body">
                                    <p><?php echo $comment['content'] ?></p>
                                </div>
                            </div>
                            <hr>
                        <?php }
                    }
                    ?>
                </div>
                <?php if(isset($_SESSION['customer'])){?>
                    <form action="" method="POST">
                        <h3>Bình luận</h3>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nội dung bình luận</label>
                            <textarea class="form-control" name="content" rows="3" required></textarea>
                        </div>

                        <button type="submit" name="submit" class="btn btn-default">Gửi</button>
                    </form>
                <?php }else{?>
                    <form action="" method="POST">
                        <h3>Bình luận</h3>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Họ và tên</label>
                            <input type="text" name="user_name" class="form-control" required placeholder="Full name">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" name="email" required placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nội dung bình luận</label>
                            <textarea class="form-control" name="guest_content" rows="3" required></textarea>
                        </div>
                        <button type="submit" name="guest_submit" class="btn btn-default">Gửi</button>
                    </form>
                <?php } ?>
                <nav aria-label="Page navigation" class="e-load-more" data-product-id="<?php echo $product['id'] ?>"
                     data-url="<?php echo base_url . 'product/comment' ?>">
                    <?php if (!empty($data['count_page']) && $data['count_page'] > 1) { ?>
                        <ul class="pagination pull-right">
                            <?php for ($i = 1; $i <= $data['count_page']; $i++) { ?>
                                <li><span data-page="<?php echo $i; ?>"><?php echo $i; ?></span></li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </nav>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="page-header text-center">
        <h1>Sản phẩm không tồn tại</h1>
    </div>
<?php } ?>