<div class="row">
    <?php if (isset($data['msg'])) { ?>
        <div class="alert-info" role="alert">
            <h4 class="text-center"><?php echo $data['msg']; ?></h4>
        </div>
    <?php } ?>
    <form class="container" action="" method="POST" style="width: 300px">
        <div class="form-group">
            <label>Tài khoản(email) của bạn</label>
            <input type="email" required class="form-control" name="email" placeholder="Please Enter Email"/>
        </div>
        <button type="submit" class="btn btn-default" name="submit">Xác nhận</button>
        <button type="reset" class="btn btn-default">Reset</button>
    </form>
</div>