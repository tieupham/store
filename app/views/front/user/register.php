<div class="container" style="max-width: 500px;">
    <div class="login-panel panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Đăng kí</h3>
        </div>
        <div class="panel-body">
            <?php if (!empty($data['errors'])) { ?>
                <div class="alert alert-info">
                    <ul>
                        <?php foreach ($data['errors'] as $e) { ?>
                            <li><?php echo $e; ?></li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
            <form action="" method="POST">
                <div class="form-group">
                    <label>Tên đầy đủ</label>
                    <input class="form-control" name="name"/>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" name="email"/>
                </div>
                <div class="form-group">
                    <label>Mật khẩu</label>
                    <input type="password" class="form-control" name="password"/>
                </div>
                <div class="form-group">
                    <label>Địa chỉ</label>
                    <input class="form-control" name="address"/>
                </div>
                <div class="form-group">
                    <label>Số điện thoại</label>
                    <input type="number" class="form-control" name="phone"/>
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label><input name="notification" type="checkbox" value="1">Nhận thông báo</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-default" name="submit">Đăng kí</button>
                <button type="reset" class="btn btn-default">Reset</button>
                <form>
        </div>
    </div>
</div>