<div class="row">
    <?php if (isset($data['user'])) { ?>
        <?php if (isset($data['msg'])) { ?>
            <div class="alert-info" role="alert">
                <h4 class="text-center"><?php echo $data['msg']; ?></h4>
            </div>
        <?php } ?>
        <form class="container" action="" method="POST" style="width: 300px">
            <div class="form-group">
                <label>Nhập password mới</label>
                <input type="password" min="8" required class="form-control" name="password" placeholder="Please Enter Password"/>
            </div>
            <button type="submit" class="btn btn-default" name="submit">Xác nhận</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </form>
    <?php }else{?>
        <div class="alert-info" role="alert">
            <h3 class="text-center">Sai thông tin, vui lòng kiểm tra lại.</h3>
        </div>
    <?php } ?>
</div>