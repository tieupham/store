<div class="container login-content">
    <div class="col-md-4 col-md-offset-4">
        <div class="login-panel panel panel-default">
            <?php if (isset($msg)) { ?>
                <div class="alert alert-info"><?php echo $msg ?></div>
                <?php unset($msg);
            } ?>
            <div class="panel-heading">
                <h3 class="panel-title">Vui lòng đăng nhập</h3>
            </div>
            <div class="panel-body">
                <form role="form" action="" method="POST">
                    <fieldset>
                        <div class="form-group">
                            <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Password" name="password" type="password"
                                   value="">
                        </div>
                        <button type="submit" name="submit" class="btn btn-success btn-block">Đăng nhập</button>
                    </fieldset>
                </form>
                <a class="text-center btn btn-default btn-block" style="margin-top: 20px"
                   href="<?php echo base_url . 'user/forgot_password' ?>">Quên mật khẩu</a>
                <a class="text-center btn btn-primary btn-block" style="margin-top: 20px"
                   href="<?php echo base_url . 'user/register' ?>">Đăng kí</a>
            </div>
        </div>
    </div>
</div>
