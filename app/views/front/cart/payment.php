<?php if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])){ ?>
<div class="container" style="max-width: 500px;">
    <div class="login-panel panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Thông tin đơn hàng</h3>
        </div>
        <div class="panel-body">
            <form action="" method="POST">
                <div class="form-group">
                    <label>Tên đầy đủ</label>
                    <input class="form-control" required name="name"/>
                </div>
                <div class="form-group">
                    <label>Địa chỉ</label>
                    <input class="form-control" required name="address"/>
                </div>
                <div class="form-group">
                    <label>Số điện thoại</label>
                    <input type="number" class="form-control" required name="phone"/>
                </div>
                <div class="form-group">
                    <label style="color: #ff4422">(*)Thanh toán tại nhà</label>
                </div>
                <button type="submit" class="btn btn-default" name="submit">Xác nhận</button>
                <button type="reset" class="btn btn-default">Reset</button>
                <form>
        </div>
    </div>
</div>
<?php }else{ ?>
    <h2 class="text-center">Giỏ hàng không có sản phẩm nào.</h2>
<?php }?>