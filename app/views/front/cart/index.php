<div class="row">
    <a href="<?php echo base_url.'cart/trans_history'?>" class="btn btn-primary btn-lg pull-right" role="button">Xem lịch sử giao dịch</a>
</div>
<?php if (isset($_SESSION['cart']) && !empty(isset($_SESSION['cart']))) {
    $total_price = 0; ?>
    <div class="col-lg-9">
        <div class="panel panel-default e-data-url"
             data-url-delete="<?php echo base_url . 'cart/delete_item_of_cart' ?>"
             data-url-update="<?php echo base_url . 'cart/change_item_count' ?>" style="margin-bottom: 100px;">

            <div class="panel-heading">
                <h4>GIỎ HÀNG
                    <small>(<?php echo count($_SESSION['cart']) ?> sản phẩm)</small>
                </h4>
            </div>
            <div class="panel-body">
                <?php foreach ($_SESSION['cart'] as $product) {
                    ?>
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="col-sm-4">
                                <img class="img-responsive center-block"
                                     src="<?php echo base_url . $product['image'] ?>"
                                     alt="<?php echo $product['name'] ?>" height="90%">
                            </div>
                            <div class="col-sm-8">
                                <a href="<?php echo base_url . 'product/index/' . $product['id'] ?>"><?php echo $product['name'] ?></a>
                                <h5>
                                    Cung cấp bởi <a href="<?php echo base_url . 'home' ?>">STORE</a>
                                </h5>
                                <span class="btn btn-danger e-delete-product" data-id="<?php echo $product['id'] ?>">
                                    Xóa
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="col-sm-6">
                                <?php if ($product['sale'] > 0) {
                                    $price = $product['price'] - ($product['price'] * $product['sale']) / 100;
                                    $o_price = $product['price'];
                                } else {
                                    $price = $product['price'];
                                }
                                $total_price += $price * $product['count']; ?>
                                <strong><?php echo number_format($price) . ' đ'; ?></strong>
                                <h6 class="old-price"><?php if (isset($o_price)) echo number_format($o_price) . ' đ'; ?></h6>
                                <p class="badge">- <?php echo $product['sale'] ?>%</p>
                            </div>
                            <div class="col-sm-6">
                                <div>
                                    <input class="count-item form-control data-count-<?php echo $product['id'] ?>"
                                           data-name-1="cccc" type="number"
                                           value="<?php echo $product['count'] ?>">
                                    <small>
                                        <button class="btn btn-primary e-update-cart"
                                                data-id="<?php echo $product['id'] ?>">Cập nhật
                                        </button>
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                <?php } ?>
            </div>

        </div>
    </div>
    <div class="col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">ĐƠN HÀNG</div>
            <div class="panel-body">
                <h4>Thành tiền
                    <b><?php echo $total_price; ?> đ</b>
                </h4>
            </div>
        </div>

        <a href="<?php if (isset($_SESSION['customer']) && !empty($_SESSION['customer'])) {
            echo base_url . 'cart/payment';
        } else {
            echo base_url . 'user/login';
        } ?>" class="btn btn-primary btn-block">
            Tiến hành đặt hàng
        </a>
    </div>
<?php } else { ?>
    <h2 class="text-center">Giỏ hàng không có sản phẩm nào.</h2>
<?php } ?>