<div class="page-header __history_order" data-ajax-url="<?php echo base_url . 'cart/cancel_order' ?>">
    <h3>LỊCH SỬ GIAO DỊCH</h3>
    <?php if (isset($data['orders']) && !empty($data['orders'])) {
        foreach ($data['orders'] as $key => $order) {
            if (!empty($order['children'])) {
                ?>
                <div class="row">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title"><?php echo $order['children'][0]['user_name'] . ' - ' .
                                    date('d/m/Y', strtotime($order['children'][0]['created_on'])) . ' - ' .
                                    number_format($order['children'][0]['total_price']) . 'đ' ?>
                                <?php if (isset($order['cancel']) && $order['cancel']) { ?>
                                    <span class="btn btn-danger pull-right e-cancel-order"
                                          data-order-id="<?php echo $key ?>">Hủy
                                    </span>
                                <?php } ?>
                            </h4>

                        </div>
                        <div class="panel-body">
                            <?php foreach ($order['children'] as $detail) { ?>
                                <div class="row">
                                    <div class="col-sm-7">
                                        <div class="col-sm-4">
                                            <img class="img-responsive center-block"
                                                 src="<?php echo base_url . $detail['image'] ?>"
                                                 alt="<?php echo $detail['product_name'] ?>" height="90%">
                                        </div>
                                        <div class="col-sm-8">
                                            <a href="<?php echo base_url . 'product/index/' . $detail['product_id'] ?>"><?php echo $detail['product_name'] ?></a>
                                            <h5>
                                                Cung cấp bởi <a href="<?php echo base_url . 'home' ?>">STORE</a>
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="col-sm-6">
                                            <strong><?php echo number_format($detail['price']) . ' đ'; ?></strong>
                                        </div>
                                        <div class="col-sm-6">
                                            <div>
                                                <input class="count-item form-control" type="number" disabled
                                                       value="<?php echo $detail['count'] ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php }
        }
    } else { ?>
        <h4 class="text-center">Không có giao dịch nào</h4>
    <?php } ?>

</div>