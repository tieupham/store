<?php if (isset($data['list_cat_child']) && !empty($data['list_cat_child'])) { ?>
    <div class="row">
        <ol class="breadcrumb">
            <?php foreach ($data['list_cat_child'] as $cat) { ?>
                <li>
                    <a href="<?php echo base_url . 'category/index/' . $cat['id'] . '/' . $_SESSION['cat_parent'] ?>">
                        <?php echo $cat['name'] ?>
                    </a>
                </li>
            <?php } ?>
        </ol>
    </div>
<?php } ?>

<div class="row">
    <?php if (isset($data['list_products']) && !empty($data['list_products'])) { ?>
        <div class="panel panel-default e-data-url"
             data-url-delete="<?php echo base_url . 'cart/delete_item_of_cart' ?>"
             data-url-update="<?php echo base_url . 'cart/change_item_count' ?>" style="margin-bottom: 100px;">

            <div class="panel-heading">
                <h4>
                    <?php echo $data['category_name']['name'] ?>
                </h4>
            </div>
            <div class="panel-body container data-load-more">
                <?php foreach ($data['list_products'] as $product) { ?>
                    <a href="<?php echo base_url . 'product/index/' . $product['id']; ?>">
                        <div class="col-sm-2-4 product-thumb">
                            <div class="thumbnail">
                                <img class="img-responsive" src="<?php echo base_url . $product['image'] ?>"
                                     alt="<?php echo $product['name'] ?>" width="50%">
                                <div class="caption">
                                    <h4><?php echo $product['name'] ?></h4>
                                    <?php if ($product['sale'] > 0) {
                                        $price = $product['price'] - ($product['price'] * $product['sale']) / 100;
                                        $o_price = $product['price'];
                                    } else {
                                        $price = $product['price'];
                                    } ?>
                                    <strong><?php echo number_format($price) . ' đ'; ?></strong>
                                    <p class="old-price"><?php if (isset($o_price)) echo number_format($o_price) . ' đ'; ?></p>
                                </div>
                            </div>
                        </div>
                    </a>
                <?php } ?>
            </div>
            <span class="btn btn-primary pull-right e-load-more"
                  data-url="<?php echo base_url . 'category/ajax_get_more' ?>"
                  data-category="<?php echo isset($data['category_id']) ? $data['category_id'] : 0 ?>">
                Xem thêm
            </span>
        </div>
    <?php } ?>
</div>