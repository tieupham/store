<div class="row">
    <ul>
        <?php if (isset($data['list_products']) && !empty($data['list_products'])) {
            foreach ($data['list_products'] as $product) { ?>
                <a href="<?php echo base_url.'product/index/'.$product['id'];?>">
                    <div class="col-sm-2-4 product-thumb">
                        <div class="thumbnail">
                            <img class="img-responsive" src="<?php echo base_url . $product['image'] ?>"
                                 alt="<?php echo $product['name'] ?>" width="50%">
                            <div class="caption">
                                <h4><?php echo $product['name'] ?></h4>
                                <?php if ($product['sale'] > 0) {
                                    $price = $product['price'] - ($product['price'] * $product['sale']) / 100;
                                    $o_price = $product['price'];
                                } else {
                                    $price = $product['price'];
                                } ?>
                                <strong><?php echo number_format($price) . ' đ'; ?></strong>
                                <p class="old-price"><?php if (isset($o_price)) echo number_format($o_price) . ' đ'; ?></p>
                            </div>
                        </div>
                    </div>
                </a>
            <?php }
        } ?>
    </ul>
</div>