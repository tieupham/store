<div>
    <div class="row">
        <div class="col-sm-8">

        </div>
        <div class="col-sm-4">

        </div>
    </div>
    <div class="row">
        <?php if (isset($data['list_products']) && !empty($data['list_products'])) {
            foreach ($data['list_products'] as $product) { ?>
                <a href="<?php echo base_url . 'product/index/' . $product['id']; ?>">
                    <div class="col-sm-4 product-thumb">
                        <div class="thumbnail">
                            <img class="img-responsive" src="<?php echo base_url . $product['image'] ?>"
                                 alt="<?php echo $product['name'] ?>" width="50%">
                            <div class="caption">
                                <h4><?php echo $product['name'] ?></h4>
                                <?php if ($product['sale'] > 0) {
                                    $price = $product['price'] - ($product['price'] * $product['sale']) / 100;
                                    $o_price = $product['price'];
                                } else {
                                    $price = $product['price'];
                                } ?>
                                <strong><?php echo number_format($price) . ' đ'; ?></strong>
                                <p class="old-price"><?php if (isset($o_price)) echo number_format($o_price) . ' đ'; ?></p>
                            </div>
                        </div>
                    </div>
                </a>
            <?php }
        } ?>
    </div>
</div>
<div class="acchome">
    <div class="naviacc">
        <h2>Phụ kiện giá rẻ</h2>
        <?php if (!empty($data['list_accessories'])) {
            foreach ($data['list_accessories'] as $accessory) { ?>
                <a href="<?php echo base_url . 'category/index/' . $accessory['id'] ?>"><?php echo $accessory['name'] ?></a>
            <?php }
        } ?>
    </div>
    <div class="row">
        <?php if (isset($data['accessory_products']) && !empty($data['accessory_products'])) {
            foreach ($data['accessory_products'] as $product) { ?>
                <a href="<?php echo base_url . 'product/index/' . $product['id']; ?>">
                    <div class="col-sm-1-5">
                        <div class="thumbnail">
                            <img class="img-responsive" src="<?php echo base_url . $product['image'] ?>"
                                 alt="<?php echo $product['name'] ?>" width="65%" height="40%">
                            <div class="caption text-center">
                                <h5><?php echo $product['name'] ?></h5>
                                <?php if ($product['sale'] > 0) {
                                    $price = $product['price'] - ($product['price'] * $product['sale']) / 100;
                                    $o_price = $product['price'];
                                } else {
                                    $price = $product['price'];
                                } ?>
                                <strong><?php echo number_format($price) . ' đ'; ?></strong>
                                <h6><?php if (isset($o_price)) echo number_format($o_price) . ' đ'; ?></h6>
                            </div>
                        </div>
                    </div>
                </a>
            <?php }
        } ?>
    </div>
</div>
