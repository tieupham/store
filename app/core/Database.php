<?php

class Database
{

	private $username = null, $password = null, $host = null,$dbname,$config;
	public $connection;
	private static $instance;
	
	private function __construct()
	{
		$this->config = include('../app/config/config.php');
		$this->password = $this->config['password'];
		$this->username = $this->config['username'];
		$this->host = $this->config['hostname'];
		$this->dbname = $this->config['database'];

		$this->connection = mysqli_connect($this->host,$this->username,
			$this->password,$this->dbname);
        mysqli_set_charset($this->connection,"utf8");
		if(mysqli_connect_error()) {
			trigger_error("Failed to conencto to MySQL: " . mysql_connect_error(),
				 E_USER_ERROR);
		}
	}

	public static function getInstance() {
		if(!self::$instance) { // If no instance then make one
			self::$instance = new self();
		}
		return self::$instance;
	}

	private function __clone() {}

    private function __wakeup(){}
}