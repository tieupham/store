<?php

class App {

    protected $controller = 'home';

    protected $method = 'index';

    protected $params = [];

    public function __construct() {
        $url = $this->parseUrl();
        if (is_dir('../app/controllers/' . $url[0])) {
            if (file_exists('../app/controllers/' . $url[0] . '/' . $url[1] . '.php')) {
                $this->controller = $url[1];
                unset($url[1]);
            } else {
                die('This page is not exist!');
            }
            require_once '../app/controllers/' . $url[0] . '/' . $this->controller . '.php';
            unset($url[0]);
            if (isset($url[2])) {
                if (method_exists($this->controller, $url[2])) {
                    $this->method = $url[2];
                    unset($url[2]);
                } else {
                    die('This page is not exist!');
                }
            }
        } else {
            if (file_exists('../app/controllers/' . $url[0] . '.php')) {
                $this->controller = $url[0];
                unset($url[0]);
            } else {
                die('This page is not exist!');
            }
            require_once '../app/controllers/' . $this->controller . '.php';

            if (isset($url[1])) {
                if (method_exists($this->controller, $url[1])) {
                    $this->method = $url[1];
                    unset($url[1]);
                } else {
                    die('This page is not exist!');
                }
            }
        }

        $this->controller = new $this->controller();

        $this->params = $url ? array_values($url) : [];

        call_user_func_array([$this->controller, $this->method], $this->params);
    }

    public function parseUrl() {
        if (isset($_GET['url'])) {
            return explode('/', filter_var(trim($_GET['url'], '/'), FILTER_SANITIZE_URL));
        }
    }
}