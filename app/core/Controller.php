<?php

abstract class Controller {
    public function model($model) {
        $path = '../app/models/' . $model . '.php';
        if (file_exists($path)) {
            require_once '../app/models/' . $model . '.php';
            return new $model();
        }
        return NULL;
    }

    public function view($view, $data = Array(), $type = TRUE) {
        if ($type) {
            $header = $footer = "admin";
        } else {
            require_once '../app/models/categories.php';
            $model = new categories();
            $data['menu'] = $model->get_list_filter(['parent_id' => 0], [], [], []);
            $header = $footer = "front";
        }
        require_once '../app/views/' . $header . '/layouts/header.php';
        require_once '../app/views/' . $header . '/layouts/navbar.php';
        require_once '../app/views/' . $view . '.php';
        require_once '../app/views/' . $footer . '/layouts/footer.php';
    }

    public function auth_login() {
        if (!isset($_SESSION['user'])) {
            header('location:' . base_url . 'admin/login');
        }
    }
}