<?php

abstract class Model {

    protected $db;

    protected $table;

    function __construct() {
        require_once '../app/core/Database.php';
        $this->db = Database::getInstance()->connection;
    }

    public function insert($data = Array()) {
        if (!empty($data)) {
            $columns = implode(",", array_keys($data));
            $escaped_values = array_values($data);
            foreach ($escaped_values as $k => $v) $escaped_values[$k] = "'" . str_replace("'", "", $v) . "'";
            $values = implode(", ", $escaped_values);
            $sql = "INSERT INTO $this->table($columns) VALUES ($values)";
            $r = mysqli_query($this->db, $sql) or die("Query: " . $sql . mysqli_error($this->db));
            if ($r) {
                $this->write_sql_log($sql);
                return mysqli_insert_id($this->db);
            }
            return FALSE;
        }
        return FALSE;
    }

    public function update($id = NULL, $data = Array()) {
        if (!empty($data) && $id) {
            $set = "";
            foreach ($data as $key => $value) {
                $set .= $key . "='" . $value . "',";
            }
            $set = rtrim($set, ",");
            $sql = "UPDATE $this->table SET $set WHERE id=$id";
            $r = mysqli_query($this->db, $sql) or die("Query: " . $sql . mysqli_error($this->db));
            if ($r) {
                $this->write_sql_log($sql);
                return $r;
            }
            return FALSE;
        }

        return FALSE;
    }

    public function delete($id = NULL) {
        if ($id) {
            $sql = "UPDATE $this->table SET deleted = 1 WHERE id = $id";
            $r = $this->db->query($sql) or die("Query: " . $sql . mysqli_error($this->db));
            if ($r) {
                $this->write_sql_log($sql);
                return $r;
            }
            return FALSE;
        }
        return FALSE;
    }

    public function update_child($whereCondition = NULL) {
        $condition = "";
        if (is_array($whereCondition) && count($whereCondition) > 0) {
            foreach ($whereCondition as $key => $value) {
                $condition .= " " . $key . " = '" . $value . "' ,";
            }
            $condition = rtrim($condition, ",");
            $sql = "UPDATE $this->table SET deleted = 1 WHERE $condition";
            $r = $this->db->query($sql) or die("Query: " . $sql . mysqli_error($this->db));
            if ($r) {
                $this->write_sql_log($sql);
                return $r;
            }
            return FALSE;
        }
        return FALSE;
    }

    public function get_one_value($id, $field = NULL) {
        if (is_array($field) && count($field) > 0) {
            $_field = rtrim(implode(',', $field), ",");
            $sql = "SELECT $_field FROM $this->table WHERE id = $id AND deleted =0 LIMIT 1";
        } else {
            $sql = "SELECT * FROM $this->table WHERE id = $id AND deleted =0 LIMIT 1";
        }
        $r = mysqli_query($this->db, $sql) or die("Query: " . $sql . mysqli_error($this->db));
        if ($r) {
            $this->write_sql_log($sql);
            return mysqli_fetch_assoc($r);
        }
        return NULL;
    }

    public function get_one_from_field($field = NULL, $data = NULL) {
        $sql = "SELECT * FROM $this->table WHERE $field = '$data' AND deleted =0 LIMIT 1";
        $r = mysqli_query($this->db, $sql) or die("Query: " . $sql . mysqli_error($this->db));
        if ($r) {
            $this->write_sql_log($sql);
            return mysqli_fetch_assoc($r);
        }
        return NULL;
    }

    public function get_list_filter($whereCondition, $whereInCondition, $likeCondition, $field, $limit = 0, $post = 0, $order = NULL) {
        $sql = $this->_filter_prepare($whereCondition, $whereInCondition, $likeCondition, $field, $limit, $post, $order);
        $r = mysqli_query($this->db, $sql) or die("Query: " . $sql . mysqli_error($this->db));
        if ($r) {
            $this->write_sql_log($sql);
            return mysqli_fetch_all($r, MYSQLI_ASSOC);
        }
        return NULL;
    }

    private function _filter_prepare($whereCondition, $whereInCondition, $likeCondition, $field, $limit = 0, $post = 0, $order = NULL) {
        $condition = "deleted = 0 AND";
        if (is_array($whereCondition) && count($whereCondition) > 0) {
            $temp = NULL;
            foreach ($whereCondition as $key => $value) {
                $temp .= " " . $key . " = '" . $value . "' AND";
            }
            $condition .= $temp;
        }

        if (is_array($whereInCondition) && count($whereInCondition) > 0) {
            $temp = Array();
            foreach ($whereInCondition as $key => $value) {
                if (is_array($value) && count($value) > 0) {
                    foreach ($value as $k => $v) $value[$k] = "'" . $v . "'";
                    $temp[] = " " . $key . " IN (" . implode(',', $value) . ")";
                }
            }

            if (count($temp) > 0) {
                foreach ($temp as $v) {
                    $condition .= " " . $v . " AND";
                }
            }
        }

        if (is_array($likeCondition) && count($likeCondition) > 0) {
            $temp = NULL;
            foreach ($likeCondition as $key => $value) {
                $temp .= " " . $key . " LIKE '" . $value . "' AND";
            }
            $condition .= $temp;
        }

        $condition = rtrim($condition, ' AND');
        if ($order) {
            $condition .= " ORDER BY " . $order . ' ASC';
        } else {
            $condition .= " ORDER BY id DESC";
        }

        if ($limit > 0) {
            if ($post > 0) {
                $condition .= " LIMIT " . $post . " ," . $limit;
            } else {
                $condition .= " LIMIT " . $limit;
            }
        }

        if (is_array($field) && count($field) > 0) {
            $_field = rtrim(implode(',', $field), ",");
            return "SELECT $_field FROM $this->table WHERE $condition";
        } else {
            return "SELECT * FROM $this->table WHERE $condition";
        }
    }

    public function write_sql_log($query) {
        if ($query) {
            $ip = $this->_get_client_ip();
            $access_type = $this->_get_user_agent();
            $sql = "INSERT INTO sql_log(ip,access_type,query) VALUES ('$ip','$access_type',\"$query\")";
            mysqli_query($this->db, $sql) or die("Query: " . $sql . mysqli_error($this->db));
            return mysqli_insert_id($this->db);
        } else
            die("Write sql log fail");
    }

    protected function _get_client_ip() {
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    protected function _get_user_agent() {
        $iPod = strpos($_SERVER['HTTP_USER_AGENT'], "iPod");
        $iPhone = strpos($_SERVER['HTTP_USER_AGENT'], "iPhone");
        $iPad = strpos($_SERVER['HTTP_USER_AGENT'], "iPad");
        $android = strpos($_SERVER['HTTP_USER_AGENT'], "Android");
        if ($iPad || $iPhone || $iPod) {
            return 2;
        } else if ($android) {
            return 3;
        } else {
            return 1;
        }
    }
}