<?php

class User extends Controller {
    protected $model, $m_user_group, $login_log, $mail;

    public function __construct() {
        $this->model = $this->model('users');
        $this->m_user_group = $this->model('user_group');
        $this->login_log = $this->model('login_log');
        $this->mail = $this->model('mail');
    }

    public function login() {
        $data = Array();
        if (isset($_SESSION['customer'])) {
            header('location:' . base_url . 'home');
        }
        if (isset($_POST['submit'])) {
            $email = htmlentities($_POST['email']);
            $password = md5(htmlentities($_POST['password']) . '123123');
            $user = $this->model->get_info_user_login($email, $password);
            if ($user) {
                $_SESSION['customer'] = $user;
                if (!$this->_write_login_log($user['id'])) die("Write login log fail");
                header('location:' . base_url . 'home');
            } else {
                $data['msg'] = "Email hoặc mật khẩu không đúng.";
            }
        }
        $this->view('front/user/login', $data, FALSE);
    }

    public function logout() {
        unset($_SESSION['customer']);
        header('location:' . base_url . 'home');
    }

    public function register() {
        $data = Array(
            'errors' => Array(),
        );
        if (isset($_POST['submit'])) {
            $validate = $this->_validate();
            $data['errors'] = $validate['errors'];
            if (empty($validate['errors'])) {
                $validate['data']['token'] = bin2hex(openssl_random_pseudo_bytes(16));
                if ($this->_save_add($validate['data'])) {
                    $_SESSION['msg'] = "Add new user success.";
                    $user = $this->model->get_info_user_login($validate['data']['email'], $validate['data']['password']);
                    if ($user) {
                        $_SESSION['customer'] = $user;
                        if (!$this->_write_login_log($user['id'])) die("Write login log fail");
                        header('location:' . base_url . 'home');
                    } else {
                        header('location:' . base_url . 'user/login');
                    }
                }
            } else {
                $data['errors'] = $validate['errors'];
            }
        }
        $this->view('front/user/register', $data, FALSE);
    }

    private function _save_add($data) {
        $id_user = $this->model->insert($data);
        if ($id_user) {
            if ($this->m_user_group->insert(['user_id' => $id_user, 'group_id' => 3])) {
                return TRUE;
            }
        }
        return FALSE;
    }

    private function _validate($disable = TRUE) {
        $data_return = Array(
            'errors' => Array(),
            'data'   => Array(),
        );
        if (isset($_POST['name']) && $_POST['name'] != '') {
            $data_return['data']['name'] = htmlentities($_POST['name']);
        } else {
            $data_return['errors'][] = "The full name is not valid";
        }
        if ($disable) {
            $check_email = $this->_check_email_exist($_POST['email']);
            if (isset($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                if ($check_email) {
                    $data_return['errors'][] = "The email is existed";
                } else {
                    $data_return['data']['email'] = $_POST['email'];
                }
            } else {
                $data_return['errors'][] = "The email is not valid";
            }
            if (isset($_POST['password'])) {
                if (strlen(htmlentities($_POST['password'])) >= 8) {
                    $data_return['data']['password'] = md5(htmlentities($_POST['password']) . '123123');
                } else {
                    $data_return['errors'][] = "The password must be at least 8 characters";
                }
            } else {
                $data_return['errors'][] = "The password is not valid";
            }
        }
        if (isset($_POST['address']) && $_POST['address'] != '') {
            $data_return['data']['address'] = htmlentities($_POST['address']);
        } else {
            $data_return['errors'][] = "The adress is not valid";
        }
        if (isset($_POST['phone']) && $_POST['phone'] != '') {
            $data_return['data']['phone'] = htmlentities($_POST['phone']);
        } else {
            $data_return['errors'][] = "The phone is not valid";
        }
        if (isset($_POST['notification']) && $_POST['notification'] == 1) {
            $data_return['data']['notification'] = 1;
        }
        return $data_return;
    }

    public function forgot_password() {
        $data = Array();
        if (isset($_POST['submit'])) {
            $user = $this->model->get_one_from_field('email', $_POST['email']);
            if ($user) {
                if ($this->mail->get_password($user)) {
                    $data['msg'] = "Vui lòng check email và làm theo yêu cầu";
                } else
                    $data['msg'] = "Gửi mail thất bại";
            } else {
                $data['msg'] = "Email không chính xác";
            }
        }
        $this->view('front/user/forgot_password', $data, FALSE);
    }

    public function forgot($email, $token) {
        $data = Array();
        if ($email && $token) {
            $user = $this->model->get_list_filter(['email' => htmlentities($email), 'token' => htmlentities($token)], [], [], []);
            if ($user) {
                $user = $user[0];
                $data['user'] = $user;
            }
            if (isset($_POST['submit'])) {
                if ($this->_reset_password($user)) {
                    header('location:' . base_url . 'user/login');
                } else {
                    $data['msg'] = "Thông tin điền không hợp lệ";
                }
            }
        }
        $this->view('front/user/new_password', $data, FALSE);
    }

    private function _reset_password($user) {
        $password = md5(htmlentities($_POST['password']) . '123123');
        return $this->model->update($user['id'], ['password' => $password, 'token' => bin2hex(openssl_random_pseudo_bytes(16))]);
    }

    private function _check_email_exist($data) {
        return $this->model->get_one_from_field('email', $data);
    }

    private function _write_login_log($user_id = NULL) {
        $data_insert = Array(
            'ip'          => $this->_get_client_ip(),
            'access_type' => $this->_get_user_agent(),
        );
        if ($user_id) {
            $data_insert['user_id'] = $user_id;
            $data_insert['login_result'] = 1;
        } else
            $data_insert['login_result'] = 2;

        return $this->login_log->insert($data_insert);
    }

    private function _get_client_ip() {
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    private function _get_user_agent() {
        $iPod = strpos($_SERVER['HTTP_USER_AGENT'], "iPod");
        $iPhone = strpos($_SERVER['HTTP_USER_AGENT'], "iPhone");
        $iPad = strpos($_SERVER['HTTP_USER_AGENT'], "iPad");
        $android = strpos($_SERVER['HTTP_USER_AGENT'], "Android");
        if ($iPad || $iPhone || $iPod) {
            return 2;
        } else if ($android) {
            return 3;
        } else {
            return 1;
        }
    }
}