<?php

class Product extends Controller {

    protected $model, $category, $producer;

    public function __construct() {
        $this->auth_login();
        $this->model = $this->model('products');
        $this->category = $this->model('categories');
        $this->producer = $this->model('producers');
    }

    public function index() {
        $data = Array();
        $data['products'] = $this->model->get_list_products();
        $this->view('admin/product/list_product', $data);
    }

    public function add() {
        $data = Array(
            'errors' => Array(),
        );
        $data['categories'] = $this->category->get_list_filter([], [], [], []);
        $data['producers'] = $this->producer->get_list_filter([], [], [], []);
        if (isset($_POST['submit'])) {
            $validate = $this->_validate();
            if (empty($validate['errors'])) {
                $path = $this->_handle_file();
                if ($path) {
                    $validate['data']['image'] = $path;
                    if ($this->model->insert($validate['data'])) {
                        $_SESSION['msg'] = "Add new product success.";
                        header('location:' . base_url . 'admin/product');
                    } else {
                        $data['errors'][] = "Somethings are wrong! Please check it again.";
                    }
                } else {
                    $data['errors'][] = 'File image is not valid';
                }
            } else {
                $data['errors'] = $validate['errors'];
            }
        }

        $this->view('admin/product/add_new', $data);
    }

    public function edit($id=NULL) {
        $data = Array(
            'errors' => Array(),
        );
        $data['categories'] = $this->category->get_list_filter([], [], [], []);
        $data['producers'] = $this->producer->get_list_filter([], [], [], []);
        $data['product'] = $this->model->get_one_product($id);
        if (isset($_POST['submit'])) {
            $validate = $this->_validate();
            if (empty($validate['errors'])) {
                $path = $this->_handle_file();
                if ($path) {
                    unlink($data['product']['image']);
                    $validate['data']['image'] = $path;
                }
                if ($this->model->update($id, $validate['data'])) {
                    $_SESSION['msg'] = "Edit product success.";
                    header('location:' . base_url . 'admin/product');
                } else {
                    $data['errors'][] = "Somethings are wrong! Please check it again.";
                }
            } else {
                $data['errors'] = $validate['errors'];
            }
        }
        $this->view('admin/product/edit', $data);
    }

    public function delete($id =NULL) {
        $id = htmlentities($id);
        if ($this->model->delete($id)) {
            $_SESSION['msg'] = "Delete product success.";
        } else {
            $_SESSION['msg'] = "Delete product fail.";
        }
        header('location:' . base_url . 'admin/product');
    }

    private function _handle_file() {
        if ($_FILES['image']['name'] != NULL) {
            $imageFileType = $_FILES["image"]['type'];
            if ($imageFileType == "image/jpg" ||
                $imageFileType == "image/png" ||
                $imageFileType == "image/jpeg" ||
                $imageFileType == "image/gif"
            ) {
                $image_name = time() . $_FILES['image']['name'];
                $path = 'upload/product/' . $image_name;
                if (move_uploaded_file($_FILES['image']['tmp_name'], $path)) {
                    return $path;
                }
            }
        }
        return FALSE;
    }

    private function _validate() {
        $data_return = Array(
            'errors' => Array(),
            'data'   => Array(),
        );
        if (isset($_POST['name']) && $_POST['name'] != '') {
            $data_return['data']['name'] = htmlentities($_POST['name']);
        } else {
            $data_return['errors'][] = "The product name is not valid";
        }
        if (isset($_POST['price']) && $_POST['price'] > 0) {
            $data_return['data']['price'] = $_POST['price'];
        } else {
            $data_return['errors'][] = "The price is not valid";
        }
        if (isset($_POST['sale']) && $_POST['sale'] < 100) {
            $data_return['data']['sale'] = $_POST['sale'];
        } else {
            $data_return['errors'][] = "The sale is not valid";
        }
        if (isset($_POST['quantity']) && $_POST['quantity'] > 0) {
            $data_return['data']['quantity'] = $_POST['quantity'];
        } else {
            $data_return['errors'][] = "The quantity is not valid";
        }
        $data_return['data']['description'] = $_POST['description'];
        $data_return['data']['category_id'] = $_POST['category_id'];
        $data_return['data']['producer_id'] = $_POST['producer_id'];
        return $data_return;
    }
}