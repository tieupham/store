<?php

class Order extends Controller {

    protected $order, $order_detail;

    public function __construct() {
        $this->auth_login();
        $this->order = $this->model('orders');
        $this->order_detail = $this->model('order_details');
    }

    public function index() {
        $data = Array();
        $data['js'][]= 'admin_asset/js/order.js';
        $data['list_order'] = $this->order->get_list_filter([], [], [], []);
        $this->view('admin/order/list_order', $data);
    }

    public function detail($order_id) {
        $data = Array();
        $data['order_details'] = $this->order_detail->get_list_order_details($order_id);
        $this->view('admin/order/order_detail', $data);
    }

    public function confirm_order(){
        $data_return = Array(
            'status'=>FALSE,
            'message'=>"Xác nhận thất bại"
        );
        if(isset($_POST['order_id']) && is_numeric(htmlentities($_POST['order_id']))){
            if($this->order->update($_POST['order_id'],['status'=>1])){
                $data_return['status']=TRUE;
                $data_return['message']="Đã xác nhận đơn hàng";
            }
        }
        echo json_encode($data_return);
    }
}