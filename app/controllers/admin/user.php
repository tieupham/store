<?php

class User extends Controller {
    protected $model, $m_group, $m_user_group;

    public function __construct() {
        $this->auth_login();
        $this->model = $this->model('users');
        $this->m_group = $this->model('groups');
        $this->m_user_group = $this->model('user_group');
    }

    public function index() {
        $data = Array();
        $data['users'] = $this->model->get_list_user();
        $this->view('admin/user/list_user', $data);
    }

    public function add() {
        $data = Array(
            'errors' => Array(),
        );
        if (isset($_POST['submit'])) {
            $validate = $this->_validate();
            $data['errors'] = $validate['errors'];
            if (empty($validate['errors'])) {
                $validate['data']['token'] = bin2hex(openssl_random_pseudo_bytes(16));
                if ($this->_save_add($validate['data'])) {
                    $_SESSION['msg'] = "Add new user success.";
                    header('location:' . base_url . 'admin/user');
                }
            } else {
                $data['errors'] = $validate['errors'];
            }
        }
        $data['groups'] = $this->m_group->get_list_filter([], [], [], ['id', 'name']);
        $this->view('admin/user/add_new', $data);
    }

    public function edit($id=NULL) {
        $data = Array(
            'errors' => Array(),
        );
        $data['groups'] = $this->m_group->get_list_filter([], [], [], ['id', 'name']);
        $data['user'] = $this->model->get_one_info_user($id);
        if (isset($_POST['submit'])) {
            $validate = $this->_validate(FALSE);
            $data['errors'] = $validate['errors'];
            if (isset($_POST['password']) && $_POST['password'] != '') {
                if (strlen(htmlentities($_POST['password'])) >= 8) {
                    $validate['data']['password'] = md5(htmlentities($_POST['password']) . '123123');
                } else {
                    $validate['errors'][] = "The password must be at least 8 characters";
                }
            }
            if (empty($validate['errors'])) {
                if ($this->model->update($id,$validate['data']) && $this->m_user_group->update($data['user']['user_group_id'],['group_id' => $_POST['group_id']])) {
                    $_SESSION['msg'] = "Edit user success.";
                    header('location:' . base_url . 'admin/user');
                }
            } else {
                $data['errors'] = $validate['errors'];
            }
        }
        $this->view('admin/user/edit', $data);
    }

    public function delete($id=NULL) {
        $id = htmlentities($id);
        if ($this->model->delete($id)) {
            $_SESSION['msg'] = "Delete producer success.";
        } else {
            $_SESSION['msg'] = "Delete producer fail.";
        }
        header('location:' . base_url . 'admin/user');
    }

    private function _save_add($data) {
        $id_user = $this->model->insert($data);
        if ($id_user) {
            if ($this->m_user_group->insert(['user_id' => $id_user, 'group_id' => $_POST['group_id']])) {
                return TRUE;
            }
        }
        return FALSE;
    }

    private function _validate($disable = TRUE) {
        $data_return = Array(
            'errors' => Array(),
            'data'   => Array(),
        );
        if (isset($_POST['name']) && $_POST['name'] != '') {
            $data_return['data']['name'] = htmlentities($_POST['name']);
        } else {
            $data_return['errors'][] = "The full name is not valid";
        }
        if ($disable) {
            $check_email = $this->_check_email_exist('email', $_POST['email']);
            if (isset($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                if ($check_email) {
                    $data_return['errors'][] = "The email is existed";
                } else {
                    $data_return['data']['email'] = $_POST['email'];
                }
            } else {
                $data_return['errors'][] = "The email is not valid";
            }
            if (isset($_POST['password'])) {
                if (strlen(htmlentities($_POST['password'])) >= 8) {
                    $data_return['data']['password'] = md5(htmlentities($_POST['password']) . '123123');
                } else {
                    $data_return['errors'][] = "The password must be at least 8 characters";
                }
            } else {
                $data_return['errors'][] = "The password is not valid";
            }
        }
        if (isset($_POST['address']) && $_POST['address'] != '') {
            $data_return['data']['address'] = htmlentities($_POST['address']);
        } else {
            $data_return['errors'][] = "The adress is not valid";
        }
        if (isset($_POST['phone']) && $_POST['phone'] != '') {
            $data_return['data']['phone'] = htmlentities($_POST['phone']);
        } else {
            $data_return['errors'][] = "The phone is not valid";
        }
        if (is_numeric(htmlentities($_POST['group_id'])) == FALSE) {
            $data_return['errors'][] = "The group is not valid";
        }
        return $data_return;
    }

    private function _check_email_exist($file, $data) {
        return $this->model->get_one_from_field($file, $data);
    }
}