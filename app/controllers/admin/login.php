<?php

class Login extends Controller {

    protected $model;

    public function __construct() {
        $this->model = $this->model('users');
    }

    public function index() {
        if (isset($_SESSION['user'])) {
            header('location:' . base_url . 'admin/user');
        }
        if (isset($_POST['submit'])) {
            $email = htmlentities($_POST['email']);
            $password = md5(htmlentities($_POST['password']) . '123123');
            $user = $this->model->get_info_user_login($email, $password);
            if ($user) {
                $_SESSION['user'] = $user;
                header('location:' . base_url . 'admin/user');
            }
        }
        require_once '../app/views/admin/login.php';
    }

    public function logout() {
        unset($_SESSION['user']);
            header('location:' . base_url . 'admin/login');
    }
}