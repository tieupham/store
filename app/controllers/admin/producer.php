<?php

class Producer extends Controller {

    protected $model;

    public function __construct() {
        $this->auth_login();
        $this->model = $this->model('producers');
    }

    public function index() {
        $data = Array();
        $data['producers'] = $this->model->get_list_filter([], [], [], []);
        $this->view('admin/producer/list_producer', $data);
    }

    public function add() {
        $data = Array(
            'errors' => Array(),
        );
        $validate = $this->_validate();
        if (isset($_POST['submit'])) {
            if (empty($validate['errors'])) {
                if ($this->model->insert($validate['data'])) {
                    header('location:' . base_url . 'admin/producer');
                } else {
                    $data['errors'][] = "Somethings are wrong! Please check it again.";
                }
            } else {
                $data['errors'] = $validate['errors'];
            }
        }
        $this->view('admin/producer/add_new', $data);
    }

    public function edit($id=NULL) {
        $id = htmlentities($id);
        $data = Array(
            'errors' => Array(),
        );
        $data['producer'] = $this->model->get_one_value($id);
        if (isset($_POST['submit'])) {
            $validate = $this->_validate();
            $data['errors'] = $validate['errors'];
            if (empty($validate['errors'])) {
                if ($this->model->update($id, $validate['data'])) {
                    $_SESSION['msg'] = "Edit producer success.";
                    header('location:' . base_url . 'admin/producer');
                } else {
                    $data['errors'][] = "Somethings are wrong! Please check it again.";
                }
            } else {
                $data['errors'] = $validate['errors'];
            }
        }
        $this->view('admin/producer/edit', $data);
    }

    public function delete($id=NULL) {
        $id = htmlentities($id);
        if ($this->model->delete($id)) {
            $_SESSION['msg'] = "Delete producer success.";
        } else {
            $_SESSION['msg'] = "Delete producer fail.";
        }
        header('location:' . base_url . 'admin/producer');
    }

    private function _validate() {
        $data_return = Array(
            'errors' => Array(),
            'data'   => Array(),
        );
        if (isset($_POST['name']) && $_POST['name'] != '') {
            $data_return['data']['name'] = htmlentities($_POST['name']);
        } else {
            $data_return['errors'][] = "The Producer Name is not valid";
        }

        return $data_return;
    }
}