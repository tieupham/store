<?php

class Category extends Controller {

    protected $model;

    public function __construct() {
        $this->auth_login();
        $this->model = $this->model('categories');
    }

    public function index() {
        $data = Array();
        $data['list_categories'] = $this->model->get_list_filter([], [], [], []);
        $this->view('admin/category/list_category', $data);
    }

    public function add() {
        $data = Array(
            'errors' => Array(),
        );
        $data['list_categories'] = $this->model->get_list_filter(['parent_id'=>0], [], [], []);
        if (isset($_POST['submit'])) {
            $validate = $this->_validate();
            $data['errors'] = $validate['errors'];
            if (empty($validate['errors'])) {
                if ($this->model->insert($validate['data'])) {
                    $_SESSION['msg'] = "Add new category success.";
                    header('location:' . base_url . 'admin/category');
                } else {
                    $data['errors'][] = "Somethings are wrong! Please check it again.";
                }
            } else {
                $data['errors'] = $validate['errors'];
            }
        }
        $this->view('admin/category/add_new', $data);
    }

    public function delete($id = NULL) {
        $id = htmlentities($id);
        if ($this->model->delete($id)) {
            $_SESSION['msg'] = "Delete category success.";
        } else {
            $_SESSION['msg'] = "Delete category fail.";
        }
        header('location:' . base_url . 'admin/category');
    }

    public function edit($id= NULL) {
        $id = htmlentities($id);
        $data = Array(
            'errors' => Array(),
        );
        $data['list_categories'] = $this->model->get_list_filter(['parent_id'=>0], [], [], []);
        $data['category'] = $this->model->get_one_value($id);
        if (isset($_POST['submit'])) {
            $validate = $this->_validate();
            $data['errors'] = $validate['errors'];
            if (empty($validate['errors'])) {
                if ($this->model->update($id, $validate['data'])) {
                    $_SESSION['msg'] = "Edit category success.";
                    header('location:' . base_url . 'admin/category');
                } else {
                    $data['errors'][] = "Somethings are wrong! Please check it again.";
                }
            } else {
                $data['errors'] = $validate['errors'];
            }
        }
        $this->view('admin/category/edit', $data);
    }

    private function _validate() {
        $data_return = Array(
            'errors' => Array(),
            'data'   => Array(),
        );
        if (isset($_POST['name']) && $_POST['name'] != '') {
            $data_return['data']['name'] = htmlentities($_POST['name']);
        } else {
            $data_return['errors'][] = "The Category Name is not valid";
        }
        $parent_id = $_POST['parent_id'];
        if (is_numeric($parent_id)) {
            $data_return['data']['parent_id'] = $parent_id;
        } else {
            $data_return['errors'][] = "The Category Parent is not valid";
        }

        return $data_return;
    }
}