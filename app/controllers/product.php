<?php
define('record', 3);

class Product extends Controller {
    protected $product, $category, $comment;

    public function __construct() {
        $this->product = $this->model('products');
        $this->category = $this->model('categories');
        $this->comment = $this->model('comments');
    }

    public function index($id = NULL, $page = 1) {
        $data = Array();
        $data['css'][] = 'css/product.css';
        $data['js'][] = 'js/product.js';
        $id = htmlentities($id);
        if (is_numeric($id)) {
            $data['product'] = $this->product->get_one_product($id);
            $data['product_involve'] = $this->product->get_list_filter(['category_id' => $data['product']['category_id']], [], [], [], 5);
            $start = ($page - 1) * record;
            $data['comments'] = $this->comment->get_list_filter(['product_id' => $id], [], [], [], record, $start, 'created_on');
            $count = $this->comment->get_count($id);
            $count = $count['count'];
            $data['count_page'] = ceil($count / record);
            if (isset($_POST['guest_submit']) || isset($_POST['submit'])) {
                $data['msg'] = "Đã xảy ra sự cố, vui lòng kiểm tra lại.";
                if ($this->_save_comment($id)) {
                    $data['msg'] = "Bình luận của bạn đã gửi thành công.";
                } else {
                    $data['msg'] = "Gửi bình luận thất bại.";
                }
            }
        }
        $this->view('front/product/index', $data, FALSE);
    }

    public function comment() {
        $data_return = Array(
            'status' => FALSE,
        );
        if (isset($_POST['product_id']) && isset($_POST['page'])) {
            $product_id = htmlentities($_POST['product_id']);
            $page = htmlentities($_POST['page']);
            $start = ($page - 1) * record;
            $comments = $this->comment->get_list_filter(['product_id' => $product_id], [], [], [], record, $start, 'created_on');
            $data_return['status'] = TRUE;
            $text = "";
            foreach ($comments as $comment) {
                $text .= '<div class="media">
                            <div class="media-left media-object">
                                <h4>' . $comment['user_name'] . '</h4>
                                <p>' . date('d/m/Y', strtotime($comment['created_on'])) . '</p>
                            </div>
                            <div class="media-body">
                                <p>' . $comment['content'] . '</p>
                            </div>
                        </div>';
            }
            $data_return['data'] = $text;
        }
        echo json_encode($data_return);
    }

    public function search() {
        $data = Array();
        $data['css'][] = 'css/category.css';
        if (isset($_POST['submit'])) {
            $key_word = "%" . trim(htmlentities($_POST['key_word'])) . "%";
            $data['list_products'] = $this->product->get_list_search($key_word);
            $data['key_word'] = htmlentities($_POST['key_word']);
        }
        $this->view("front/product/search", $data, FALSE);
    }

    private function _save_comment($product_id) {
        if (isset($_POST['content']) && isset($_SESSION['customer'])) {
            $data = Array(
                'content'    => htmlentities($_POST['content']),
                'user_id'    => $_SESSION['customer']['id'],
                'user_name'  => $_SESSION['customer']['name'],
                'product_id' => $product_id,
            );
            return $this->comment->insert($data);
        } elseif (isset($_POST['guest_submit'])) {
            $data = Array(
                'content'    => htmlentities($_POST['guest_content']),
                'product_id' => $product_id,
                'email'      => htmlentities($_POST['email']),
                'user_name'  => htmlentities($_POST['user_name']),
            );
            return $this->comment->insert($data);
        }
        return FALSE;
    }

}