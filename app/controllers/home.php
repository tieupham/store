<?php

class Home extends Controller {

    protected $product, $category, $producer;

    public function __construct() {
        $this->product = $this->model('products');
        $this->category = $this->model('categories');
    }

    public function index() {
        $data = Array();
        $data['css'][] = 'css/home.css';
        $data['list_products'] = $this->product->get_list_products(12);
        $data['list_accessories'] = $this->category->get_list_filter(['parent_id' => 5], [], [], []);
        $whereIn = Array();
        if ($data['list_accessories']) {
            foreach ($data['list_accessories'] as $item) {
                $whereIn[] = $item['id'];
            }
        }
        $data['accessory_products'] = $this->product->get_list_filter([], ['category_id' => $whereIn], [], [], 8, 0, 'price');
        $this->view('front/home/index', $data, FALSE);
    }

    public function mail(){
        $mail=$this->model('mail');
        $mail->send();
    }
}