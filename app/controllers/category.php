<?php

define('record', 2);

class Category extends Controller {
    protected $product, $category, $producer;

    public function __construct() {
        $this->product = $this->model('products');
        $this->category = $this->model('categories');
        $this->producer = $this->model('producers');
    }

    public function index($id = NULL, $parent_id = NULL) {
        $data = Array();
        $data['css'][] = 'css/category.css';
        $data['js'][] = 'js/category.js';
        $id = htmlentities($id);
        if (is_numeric($id)) {
            if ($parent_id && is_numeric(htmlentities($parent_id))) {
                $data['list_cat_child'] = $this->category->get_list_filter(['parent_id' => $parent_id], [], [], []);
            } else {
                $data['list_cat_child'] = $this->category->get_list_filter(['parent_id' => $id], [], [], []);
            }
            if (!empty($data['list_cat_child'])) $_SESSION['cat_parent'] = $id;
            $data['list_products'] = $this->product->get_list_products_from_cat_id($id, [], record);
            $data['category_id'] = $id;
            $data['category_name'] = $this->category->get_one_value($id, ['name', 'parent_id']);
            if ($data['category_name']['parent_id'] != 0) {
                $_SESSION['cat_parent'] = $data['category_name']['parent_id'];
            }
        }
        $this->view('front/category/index', $data, FALSE);
    }

    public function ajax_get_more() {
        $data_return = Array(
            'status' => FALSE,
        );
        if (isset($_POST['category_id']) && isset($_POST['page'])) {
            $id = htmlentities($_POST['category_id']);
            $page = htmlentities($_POST['page']);
            if (is_numeric($id) && is_numeric($page)) {
                $start = ($page - 1) * record;
                $products = $this->product->get_list_products_from_cat_id($id, [], record, $start);
                if ($products) {
                    $products = $this->_format_data($products);
                    $data_return['products'] = $products;
                    $data_return['status'] = TRUE;
                }
            }
        }
        echo json_encode($data_return);
    }

    private function _format_data($products) {
        foreach ($products as $key => $product) {
            if ($product["sale"] > 0) {
                $products[$key]["old_price"] = number_format($product["price"]);
                $products[$key]["price"] = number_format($product["price"] - ($product["price"] * $product["sale"]) / 100);
            } else {
                $products[$key]["old_price"] = "";
            }
            $products[$key]["url"] = base_url;
        }
        return $products;
    }
}