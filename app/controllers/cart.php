<?php

class Cart extends Controller {

    protected $product, $order, $order_detail;

    public function __construct() {
        $this->product = $this->model('products');
        $this->order = $this->model('orders');
        $this->order_detail = $this->model('order_details');
    }

    public function index() {
        $data = Array();
        $data['css'][] = 'css/cart.css';
        $data['js'][] = 'js/cart.js';
        $this->view('front/cart/index', $data, FALSE);
    }

    public function change_item_count() {
        $data_return = Array(
            'status' => FALSE,
        );
        if (isset($_POST['key']) && isset($_POST['new_count'])) {
            if (isset($_SESSION['cart']) && isset($_SESSION['cart'][$_POST['key']])) {
                if ($_POST['new_count'] > 0) {
                    $_SESSION['cart'][$_POST['key']]['count'] = $_POST['new_count'];
                } else {
                    unset($_SESSION['cart'][$_POST['key']]);
                }
                $data_return['status'] = TRUE;
                $data_return['message'] = "Cap nhat gio hang thanh cong.";
            } else {
                $data_return['message'] = 'Khong co du lieu gio hang.';
            }
        } else {
            $data_return['message'] = 'Khong co du lieu gui len.';
        }
        echo json_encode($data_return);
    }

    public function add_cart() {
        $data_return = Array(
            'status' => FALSE,
        );
        if (isset($_POST['id'])) {
            $id = htmlentities($_POST['id']);
            if (is_numeric($id)) {
                $product = $this->product->get_one_value(htmlentities($id));
                if ($product) {
                    if (isset($_SESSION['cart'][$id])) {
                        $_SESSION['cart'][$id]['count'] += 1;
                    } else {
                        $_SESSION['cart'][$id] = $product;
                        $_SESSION['cart'][$id]['count'] = 1;
                    }
                    $data_return['status'] = TRUE;
                    $data_return['message'] = "Them vao ro hang thanh cong.";
                } else {
                    $data_return['message'] = "San pham nay khong hop le.";
                }
            } else {
                $data_return['message'] = "Id san pham nay khong hop le.";
            }
        } else {
            $data_return['message'] = "Khong co du lieu gui len.";
        }
        echo json_encode($data_return);
    }

    public function delete_item_of_cart() {
        $data_return = Array(
            'status' => FALSE,
        );

        if (isset($_POST['key'])) {
            $key = $_POST['key'];
            if (isset($_SESSION['cart']) && isset($_SESSION['cart'][$key])) {
                unset($_SESSION['cart'][$key]);
                $data_return['status'] = TRUE;
                $data_return['message'] = "Xoa san pham thanh cong.";
            } else {
                $data_return['message'] = "San pham nay khong co trong gio hang.";
            }
        } else
            $data_return['message'] = "Khong co du lieu gui len.";

        echo json_encode($data_return);
    }

    public function payment() {
        $data = Array();
        if (isset($_POST['submit'])) {
            $data_order['user_id'] = $_SESSION['customer']['id'];
            $data_order['user_name'] = htmlentities($_POST['name']);
            $data_order['address'] = htmlentities($_POST['address']);
            $data_order['phone'] = htmlentities($_POST['phone']);
            $data_order['order_code'] = "OD" . time();
            $total_price = 0;
            $check = FALSE;
            $order_id = $this->order->insert($data_order);
            if ($order_id) {
                foreach ($_SESSION['cart'] as $product) {
                    $price = ($product['sale'] > 0) ? ($product['price'] - ($product['price'] * $product['sale'] / 100)) : $product['price'];
                    $total_price += $price;
                    $order_detail_id = $this->order_detail->insert([
                        'order_id'   => $order_id,
                        'product_id' => $product['id'],
                        'price'      => $price,
                        'count'      => $product['count'],
                    ]);
                    if (!$order_detail_id) {
                        die('fails');
                    }
                }
                if ($this->order->update($order_id, ['total_price' => $total_price])) {
                    $check = TRUE;
                }
            }
            if ($check) {
                unset($_SESSION['cart']);
                header('location:' . base_url . 'home');
            }
        }
        $this->view('front/cart/payment', $data, FALSE);
    }

    public function trans_history() {
        if (isset($_SESSION['customer'])) {
        $data = Array();
        $data['css'][] = 'css/cart.css';
        $data['js'][] = 'js/cart.js';
        $orders = $this->order->get_list_order_id($_SESSION['customer']['id']);
        $result = [];
        if ($orders) {
            foreach ($orders as $row) {
                $parent_id = $row['order_id'];
                if ($parent_id) {
                    if (empty($result[$parent_id])) {
                        $result[$parent_id] = [];
                    }
                    $parent = &$result[$parent_id];
                    if (empty($parent['children'])) {
                        $parent['children'] = [];
                    }
                    $parent['children'][] = $row;
                    $diff = (strtotime(date('Y-m-d H:i:s', time())) - strtotime($row['created_on'])) / (60 * 60);
                    if ($diff < 24) {
                        $parent['cancel'] = TRUE;
                    }
                }
            }
        }
        $data['orders'] = $result;
        $this->view('front/cart/history', $data, FALSE);
        } else {
            header('location:' . base_url . 'home');
        }
    }

    public function cancel_order() {
        $data_return = Array(
            'status' => FALSE,
        );
        if (isset($_POST['order_id']) && is_numeric(htmlentities($_POST['order_id']))) {
            if ($this->order_detail->update_child(['order_id' => $_POST['order_id']])) {
                if ($this->order->delete($_POST['order_id'])) {
                    $data_return['status'] = TRUE;
                }
            }
        }
        echo json_encode($data_return);
    }
}