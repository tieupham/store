<?php

class Order_details extends Model {
    protected $table = "order_detail";

    public function get_list_order_details($order_id) {
        $sql = "SELECT od.*,o.order_code as order_code,p.name as product_name ,p.image as image 
                FROM order_detail as od 
                JOIN products as p ON p.id = od.product_id 
                JOIN orders as o ON o.id = od.order_id
                WHERE od.order_id = $order_id AND od.deleted=0 AND p.deleted =0 AND o.deleted=0";
        $r = mysqli_query($this->db, $sql) or die("Query: " . $sql . mysqli_error($this->db));
        if ($r) {
            $this->write_sql_log($sql);
            return mysqli_fetch_all($r, MYSQLI_ASSOC);
        }
        return NULL;
    }
}