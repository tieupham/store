<?php

class Users extends Model {

    protected $table = 'users';

    public function get_list_user() {
        $sql = "SELECT u.id,u.name,u.email,u.address,u.phone,g.name as group_name ,g.id as group_id
                FROM users as u INNER JOIN user_group as ug
                ON u.id=ug.user_id INNER JOIN groups as g 
                ON g.id=ug.group_id WHERE u.deleted = 0 AND ug.deleted=0 AND g.deleted =0";
        $r = mysqli_query($this->db, $sql) or die("Query: " . $sql . mysqli_error($this->db));
        if ($r) {
            $this->write_sql_log($sql);
            return mysqli_fetch_all($r, MYSQLI_ASSOC);
        }
        return NULL;
    }

    public function get_one_info_user($id) {
        $sql = "SELECT u.id,u.name,u.email,u.address,u.phone,
                g.name as group_name , g.id as group_id,
                ug.id as user_group_id
                FROM users as u INNER JOIN user_group as ug
                ON u.id=ug.user_id INNER JOIN groups as g 
                ON g.id=ug.group_id WHERE u.id= $id AND u.deleted = 0 AND ug.deleted=0 AND g.deleted =0";
        $r = mysqli_query($this->db, $sql) or die("Query: " . $sql . mysqli_error($this->db));
        if ($r) {
            $this->write_sql_log($sql);
            return mysqli_fetch_assoc($r);
        }
        return NULL;
    }

    public function get_info_user_login($email, $password) {
        $sql = "SELECT u.id,u.name,u.email,u.address,u.phone,
                g.name as group_name , g.id as group_id,
                ug.id as user_group_id
                FROM users as u INNER JOIN user_group as ug
                ON u.id=ug.user_id INNER JOIN groups as g 
                ON g.id=ug.group_id WHERE u.email= '$email' 
                AND u.password = '$password' 
                AND u.deleted = 0 AND ug.deleted=0 
                AND g.deleted =0";
        $r = mysqli_query($this->db, $sql) or die("Query: " . $sql . mysqli_error($this->db));
        if ($r) {
            $this->write_sql_log($sql);
            return mysqli_fetch_assoc($r);
        }
        return NULL;
    }
}