<?php

class Comments extends Model {
    protected $table = 'comments';

    public function get_count($id) {
        $sql = "SELECT count(id) as count FROM $this->table WHERE deleted=0 AND product_id = $id";
        $r = mysqli_query($this->db, $sql) or die("Query: " . $sql . mysqli_error($this->db));
        if ($r) {
            $this->write_sql_log($sql);
            return mysqli_fetch_assoc($r);
        }
        return NULL;
    }
}