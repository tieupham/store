<?php

class Orders extends Model {
    protected $table = "orders";

    public function get_list_order_id($user_id) {
        $sql = "SELECT o.*,od.*,p.name as product_name ,p.image as image 
                FROM orders as o 
                JOIN order_detail as od 
                ON o.id=od.order_id 
                JOIN products as p ON p.id = od.product_id 
                WHERE o.user_id = $user_id AND o.deleted =0 AND od.deleted=0 AND p.deleted =0";
        $r = mysqli_query($this->db, $sql) or die("Query: " . $sql . mysqli_error($this->db));
        if ($r) {
            $this->write_sql_log($sql);
            return mysqli_fetch_all($r, MYSQLI_ASSOC);
        }
        return NULL;
    }
}