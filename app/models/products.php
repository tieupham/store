<?php

class Products extends Model {
    protected $table = "products";

    public function get_list_products($limit = 0, $order_by = 'DESC') {
        $sql = "SELECT p.*, c.name as category_name, pc.name as producer_name
                FROM products as p INNER JOIN categories as c
                ON c.id=p.category_id INNER JOIN producers as pc 
                ON pc.id=p.producer_id WHERE p.deleted = 0 AND c.deleted=0 AND pc.deleted =0 ORDER BY p.id $order_by";
        if ($limit != 0) $sql .= ' LIMIT ' . $limit;
        $r = mysqli_query($this->db, $sql) or die("Query: " . $sql . mysqli_error($this->db));
        if ($r) {
            $this->write_sql_log($sql);
            return mysqli_fetch_all($r, MYSQLI_ASSOC);
        }
        return NULL;
    }

    public function get_one_product($id) {
        $sql = "SELECT p.*, c.name as category_name, pc.name as producer_name
                FROM products as p INNER JOIN categories as c
                ON c.id=p.category_id INNER JOIN producers as pc 
                ON pc.id=p.producer_id WHERE p.id = $id AND p.deleted = 0 AND c.deleted=0 AND pc.deleted =0";
        $r = mysqli_query($this->db, $sql) or die("Query: " . $sql . mysqli_error($this->db));
        if ($r) {
            $this->write_sql_log($sql);
            return mysqli_fetch_assoc($r);
        }
        return NULL;
    }

    public function get_list_products_from_cat_id($cat_id = NULL, $whereCondition = Array(), $limit = 0, $start = 0, $order_by = 'DESC') {
        $condition = "";
        if (is_array($whereCondition) && count($whereCondition) > 0) {
            $temp = NULL;
            foreach ($whereCondition as $key => $value) {
                $temp .= $key . " = '" . $value . "' AND ";
            }
            $condition .= $temp;
        }
        $sql = "SELECT p.*, c.name as category_name, pc.name as producer_name
                FROM products as p INNER JOIN categories as c
                ON c.id=p.category_id INNER JOIN producers as pc 
                ON pc.id=p.producer_id WHERE c.id=$cat_id OR c.parent_id=$cat_id AND $condition p.deleted = 0 AND c.deleted=0 AND pc.deleted =0 ORDER BY p.id $order_by";
        if ($limit > 0) {
            if ($start > 0) {
                $sql .= " LIMIT " . $start . " ," . $limit;
            } else {
                $sql .= " LIMIT " . $limit;
            }
        }
        $r = mysqli_query($this->db, $sql) or die("Query: " . $sql . mysqli_error($this->db));
        if ($r) {
            $this->write_sql_log($sql);
            return mysqli_fetch_all($r, MYSQLI_ASSOC);
        }
        return NULL;
    }

    public function get_list_search($key_word) {
        $sql = "SELECT p.*, c.name as category_name, pc.name as producer_name
                FROM products as p INNER JOIN categories as c
                ON c.id=p.category_id INNER JOIN producers as pc 
                ON pc.id=p.producer_id WHERE p.name LIKE '$key_word' OR pc.name LIKE '$key_word' OR c.name LIKE '$key_word' AND p.deleted = 0 AND c.deleted=0 AND pc.deleted =0";
        $r = mysqli_query($this->db, $sql) or die("Query: " . $sql . mysqli_error($this->db));
        if ($r) {
            $this->write_sql_log($sql);
            return mysqli_fetch_all($r, MYSQLI_ASSOC);
        }
        return NULL;
    }
}